#pragma once

// Copyright(c) 2019 Brian Batista.
// Refer to LICENSE for more informations

using uint8  = unsigned char;      // Unsigned integer with 8 bits.
using uint16 = unsigned short int; // Unsigned integer with 16 bits.
using uint32 = unsigned int;       // Unsigned integer with 32 bits.
using uint64 = unsigned long long; // Unsigned integer with 64 bits.
using int8   = char;               // Signed integer with 8 bits.
using int16  = short int;          // Signed integer with 16 bits.
using int32  = int;                // Signed integer with 32 bits.
using int64  = long long;          // Signed integer with 64 bits.

#ifdef AE_WINDOWS
#define finline __forceinline
#ifdef AE_DEBUG
#define _CRTDBG_MAP_ALLOC
#include <crtdbg.h>
#include <stdlib.h>

#define DEBUG_NEW new (_NORMAL_BLOCK, __FILE__, __LINE__)
#define new DEBUG_NEW
#endif
#else
#define finline inline
#endif // AE_WINDOWS

#if defined(UNICODE) || defined(_UNICODE)
using tchar = wchar_t;
#ifdef TEXT
#undef TEXT
#endif // !TEXT
#define TEXT(t) L##t
#else
using tchar = char;
#ifdef TEXT
#undef TEXT
#endif // !TEXT
#define TEXT(t) t
#endif

#define SAFE_DELETE(x) \
    {                  \
        delete x;      \
        x = nullptr;   \
    }

#define SAFE_DELETE_ARRAY(x) \
    {                        \
        delete[] x;          \
        x = nullptr;         \
    }
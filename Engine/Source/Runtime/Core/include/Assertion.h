/**
 * @file Assertion.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2019-11-05
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once

#include <assert.h>

#define AE_ASSERT(x) assert(x)

/**
 * @file AString.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2019-10-31
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once

#include "CorePCH.pch.h"

#if defined(UNICODE) || defined(_UNICODE)
using String = std::wstring;
#else
using String = std::string;
#endif // UNICODE

namespace StringUtils {

}
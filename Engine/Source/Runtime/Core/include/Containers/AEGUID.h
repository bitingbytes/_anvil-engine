/**
 * @file AEGUID.h
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2019-11-12
 *
 * @copyright Copyright (c) 2019
 *
 */

#pragma once

#include "Containers/AString.h"
#include "CorePCH.pch.h"
#include "Platform/PlatformIncludes.h"
#include "Types.h"
#include "Utils/LibraryUtils.h"

class AE_CORE_API AEGUID
{
  public:
    static AEGUID Generate();

    String ToString(bool _useSeparator = true, bool _toUpper = true) const;

  private:
    AEGUID();

  private:
    uint32 m_data1;
    uint16 m_data2;
    uint16 m_data3;
    uint8  m_data4[8];
};
/**
 * @file Version.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2019-11-13
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once

#include "Containers/AString.h"
#include "Types.h"

struct Version
{
    union
    {
        struct
        {
            uint8  Major;
            uint8  Minor;
            uint16 Patch;
        };
        uint32 Int;
    };

    finline Version()
      : Major(0)
      , Minor(0)
      , Patch(0)
    {}

    finline Version(uint8 _major, uint8 _minor, uint16 _patch)
      : Major(_major)
      , Minor(_minor)
      , Patch(_patch)
    {}

    finline Version(const char* _version)
      : Int(0)
    {
        FromString(_version);
    }

    finline String ToString() const
    {
        auto majorStr = std::to_string(Major);
        auto minorStr = std::to_string(Minor);
        auto patchStr = std::to_string(Patch);

        auto result = majorStr + '.' + minorStr + '.' + patchStr;
        return result;
    }

    finline operator String() const { return ToString(); }

  private:
    void FromString(const char* _version)
    {
        String versionStr(_version);
        auto   period0 = versionStr.find_first_of('.');
        auto   period1 = versionStr.find_first_of('.', period0 + 1);

        auto majorStr = versionStr.substr(0, period0);
        auto minorStr = versionStr.substr(period0 + 1, (period1 - 1) - period0);
        auto patchStr = versionStr.substr(period1 + 1);

        Major = static_cast<uint8>(std::stoi(majorStr.c_str()));
        Minor = static_cast<uint8>(std::stoi(minorStr.c_str()));
        Patch = static_cast<uint16>(std::stoi(patchStr.c_str()));
    }
};
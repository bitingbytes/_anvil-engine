#pragma once
/**
 * @file Core.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2019-10-29
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once

#include "Assertion.h"
#include "Containers/AEGUID.h"
#include "Containers/AString.h"
#include "Containers/Version.h"
#include "CoreGlobals.h"
#include "CorePCH.pch.h"
#include "Logger/Logger.h"
#include "Math/AEMath.h"
#include "Modules/IModule.h"
#include "Platform/FileSystem.h"
#include "Platform/Library.h"
#include "Platform/PlatformIncludes.h"
#include "Platform/Timer.h"
#include "Types.h"
#include "Utils/DataUtils.h"
#include "Utils/LibraryUtils.h"
#include "Utils/NonCopyable.h"
#include "Utils/Parser.h"
/**
 * @file IModule.h
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2019-11-10
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once

#include "Containers/AString.h"
#include "CorePCH.pch.h"
#include "Logger/Logger.h"
#include "Utils/LibraryUtils.h"

class IModule;

class AE_CORE_API ModuleManager
{
  public:
    ~ModuleManager();

    static ModuleManager& GetInstance();

    void RegisterModule(const String& _name, IModule* _module);

    IModule* LoadModule(const String& _name);

  private:
    std::map<String, IModule*> m_registeredModules;
};

class IModule
{
  public:
    virtual ~IModule() {}

    virtual void Initialize() {}

    virtual void Shutdown() {}

    virtual void OnUnload() {}

    virtual void OnReload() {}

    virtual bool CanBeReloaded() { return true; }

    virtual bool IsGameModule() { return false; }
};

template<class T>
class StaticallyLinkedModuleDummy
{
  public:
    StaticallyLinkedModuleDummy(const char* _name) { ModuleManager::GetInstance().RegisterModule(_name, InitializeModule()); }

    IModule* InitializeModule() { return new T(); }
};

using FModuleInitializerPtr = IModule* (*)();

#ifdef AE_MONOLITHIC
#define CREATE_MODULE(Class, Name) static StaticallyLinkedModuleDummy<Class> ModuleDummy##Name(#Name)
#else
#define CREATE_MODULE(Class, Name) \
    extern "C" AE_DLLEXPORT IModule* InitializeModule() { return new Class(); }
#endif // !AE_MONOLITHIC

/**
 * @file Parser.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2019-11-12
 *
 * @copyright Copyright (c) 2019
 *
 */

#pragma once

#include "Containers/AString.h"
#include "CorePCH.pch.h"
#include "Types.h"
#include "Utils/LibraryUtils.h"

class AE_CORE_API Parser
{
  public:
    static bool HasParam(const tchar* _command, const tchar* _param);

    static bool Value(const tchar* _command, const tchar* _param, uint8& _value);
    static bool Value(const tchar* _command, const tchar* _param, uint16& _value);
    static bool Value(const tchar* _command, const tchar* _param, uint32& _value);
    static bool Value(const tchar* _command, const tchar* _param, uint64& _value);

    static bool Value(const tchar* _command, const tchar* _param, int8& _value);
    static bool Value(const tchar* _command, const tchar* _param, int16& _value);
    static bool Value(const tchar* _command, const tchar* _param, int32& _value);
    static bool Value(const tchar* _command, const tchar* _param, int64& _value);

    static bool Value(const tchar* _command, const tchar* _param, float& _value);
    static bool Value(const tchar* _command, const tchar* _param, double& _value);

    static bool Value(const tchar* _command, const tchar* _param, bool& _value, const tchar* _quotes = "\"");

    static bool Value(const tchar* _command, const tchar* _param, String& _value, const tchar* _quotes = "\"");

  private:
    static tchar* PrepareForValue(const tchar* _command, const tchar* _param);
};
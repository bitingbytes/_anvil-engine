/**
 * @file DataUtils.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2019-11-08
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once

#define _KB(x) (x * 1024)
#define _MB(x) (x * 1024 * 1024)

#define _64KB _KB(64)
#define _1MB _MB(1)
#define _2MB _MB(2)
#define _4MB _MB(4)
#define _8MB _MB(8)
#define _16MB _MB(16)
#define _32MB _MB(32)
#define _64MB _MB(64)
#define _128MB _MB(128)
#define _256MB _MB(256)

template<typename T>
inline T
AlignUpWithMask(T value, size_t mask)
{
    return (T)(((size_t)value + mask) & ~mask);
}

template<typename T>
inline T
AlignDownWithMask(T value, size_t mask)
{
    return (T)((size_t)value & ~mask);
}

template<typename T>
inline T
AlignUp(T value, size_t alignment)
{
    return AlignUpWithMask(value, alignment - 1);
}

template<typename T>
inline T
AlignDown(T value, size_t alignment)
{
    return AlignDownWithMask(value, alignment - 1);
}

template<typename T>
inline bool
IsAligned(T value, size_t alignment)
{
    return 0 == ((size_t)value & (alignment - 1));
}

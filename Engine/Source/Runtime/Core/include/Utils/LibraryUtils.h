#pragma once
/**
 * @file LibraryUtils.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2019-10-29
 *
 * @copyright Copyright (c) 2019
 *
 */

#ifdef AE_EXPORT
#ifdef AE_WINDOWS
#define AE_DLLEXPORT __declspec(dllexport)
#define AE_DLLIMPORT __declspec(dllimport)
#elif AE_LINUX
#define AE_DLLEXPORT __attribute__((visibility("default")))
#define AE_DLLIMPORT
#else
#error Platform not supported
#endif
#else
#define AE_DLLEXPORT 
#define AE_DLLIMPORT 
#endif // AE_EXPORT

#ifdef AE_WINDOWS
#define AE_DEPRECATED(msg) __declspec(deprecated("Is deprecated. " msg))
#define AE_THREAD __declspec(thread)
#elif AE_LINUX
#define AE_DEPRECATED __declspec(deprecated)
#define AE_THREAD __thread
#else
#define AE_DEPRECATED
#define AE_THREAD
#endif
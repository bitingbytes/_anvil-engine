/**
 * @file NonCopyable.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2019-11-06
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once

class INonCopyable
{
  public:
    INonCopyable()                    = default;
    INonCopyable(const INonCopyable&) = delete;
    INonCopyable& operator=(const INonCopyable&) = delete;
};
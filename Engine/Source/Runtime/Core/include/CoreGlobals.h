/**
 * @file CoreGlobals.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2019-10-31
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once

#include "CorePCH.pch.h"
#include "Logger/Logger.h"
#include "Types.h"

CREATE_LOGGER_EXTERN(LogInit, All);

extern bool AE_CORE_API g_IsInitialized;
extern bool AE_CORE_API g_IsShuttingDown;
extern bool AE_CORE_API g_IsPaused;

extern AE_CORE_API bool g_IsEditor;

extern AE_CORE_API std::thread::id g_MainThreadID;

extern AE_CORE_API bool
IsMainThread();
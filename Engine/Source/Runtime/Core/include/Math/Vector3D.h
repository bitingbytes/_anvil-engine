#pragma once
/**
 * @file Vector3D.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2019-10-28
 *
 * @copyright Copyright (c) 2019
 *
 */

#include "CorePCH.pch.h"
#include "Types.h"
#include "Assertion.h"

/**
 * @brief Data representation of a 3D vector.
 *
 */
struct Vector3D
{
    /** Vector's X component. */
    float X;

    /** Vector's Y component. */
    float Y;

    /** Vector's Z component. */
    float Z;

    /**
     * @brief Default constructor without initialization.
     *
     */
    inline Vector3D();

    /**
     * @brief Construct a Vector 3D initializing all components to a single scalar value.
     *
     * @param _xyz Scalar to set all components to.
     */
    inline explicit Vector3D(float _xyz);

    /**
     * @brief Construct a Vector 3D object setting all components.
     *
     * @param _x Sets the X component.
     * @param _y Sets the Y component.
     * @param _z Sets the Z component.
     */
    inline Vector3D(float _x, float _y, float _z);

    /**
     * @brief Construct a Vector 3D by coping another one.
     *
     * @param _other Vector to copy from.
     */
    inline Vector3D(const Vector3D& _other);

    /**
     * @brief Return a negated copy of this vector.
     *
     * @return a negated copy of this vector.
     */
    inline Vector3D operator-() const;

    /**
     * @brief Returns the addition between this vector's components and a scalar.
     *
     * @param _s Scalar to add to this.
     * @return Vector3D resulted from the addition.
     */
    inline Vector3D operator+(float _s) const;

    /**
     * @brief Returns the addition between this vector's components and a scalar.
     *
     * @param _s Scalar to add to this.
     * @return This Vector3D with resulted from the addition.
     */
    inline Vector3D& operator+=(float _s);

    /**
     * @brief Returns the addition between this vector's components and other vector's components.
     *
     * @param _v Vector to add to this.
     * @return Vector3D resulted from the addition.
     */
    inline Vector3D operator+(const Vector3D& _v) const;

    /**
     * @brief Returns the addition between this vector's components and a scalar.
     *
     * @param _v Scalar to add to this.
     * @return This Vector3D with resulted from the addition.
     */
    inline Vector3D& operator+=(const Vector3D& _v);

    /**
     * @brief Returns the subtraction between this vector's components and a scalar.
     *
     * @param _s Scalar to add to this.
     * @return Vector3D resulted from the subtraction.
     */
    inline Vector3D operator-(float _s) const;

    /**
     * @brief Returns the subtraction between this vector's components and a scalar.
     *
     * @param _s Scalar to add to this.
     * @return This Vector3D with resulted from the subtraction.
     */
    inline Vector3D& operator-=(float _s);

    /**
     * @brief Returns the subtraction between this vector's components and other vector's
     * components.
     *
     * @param _v Vector to add to this.
     * @return Vector3D resulted from the subtraction.
     */
    inline Vector3D operator-(const Vector3D& _v) const;

    /**
     * @brief Returns the subtraction between this vector's components and a scalar.
     *
     * @param _v Scalar to add to this.
     * @return This Vector3D with resulted from the subtraction.
     */
    inline Vector3D& operator-=(const Vector3D& _v);

    /**
     * @brief Returns the multiplication between this vector's components and a scalar.
     *
     * @param _s Scalar to add to this.
     * @return Vector3D resulted from the multiplication.
     */
    inline Vector3D operator*(float _s) const;

    /**
     * @brief Returns the multiplication between this vector's components and a scalar.
     *
     * @param _s Scalar to add to this.
     * @return This Vector3D with resulted from the multiplication.
     */
    inline Vector3D& operator*=(float _s);

    /**
     * @brief Returns the multiplication between this vector's components and other vector's
     * components.
     *
     * @param _v Vector to add to this.
     * @return Vector3D resulted from the multiplication.
     */
    inline Vector3D operator*(const Vector3D& _v) const;

    /**
     * @brief Returns the multiplication between this vector's components and a scalar.
     *
     * @param _v Scalar to add to this.
     * @return This Vector3D with resulted from the multiplication.
     */
    inline Vector3D& operator*=(const Vector3D& _v);

    /**
     * @brief Returns the division between this vector's components and a scalar.
     *
     * @param _s Scalar to add to this.
     * @return Vector3D resulted from the division.
     */
    inline Vector3D operator/(float _s) const;

    /**
     * @brief Returns the division between this vector's components and a scalar.
     *
     * @param _s Scalar to add to this.
     * @return This Vector3D with resulted from the division.
     */
    inline Vector3D& operator/=(float _s);

    /**
     * @brief Returns the division between this vector's components and other vector's components.
     *
     * @param _v Vector to add to this.
     * @return Vector3D resulted from the division.
     */
    inline Vector3D operator/(const Vector3D& _v) const;

    /**
     * @brief Returns the division between this vector's components and a scalar.
     *
     * @param _v Scalar to add to this.
     * @return This Vector3D with resulted from the division.
     */
    inline Vector3D& operator/=(const Vector3D& _v);

    /**
     * @brief Evaluate the dot product between this and another vectors.
     * @details The dot between \f$u=(u_x,u_y,u_z)\f$ and \f$v=(v_x,v_y,v_z)\f$ is
     * \f$u*v=u_x*v_x+u_y*v_y+u_z*v_z\f$
     * @snippet Vector3D.cpp Vector3D::Dot
     *
     * @param _v Right hand vector
     * @return Dot product between u and v
     */
    inline float operator|(const Vector3D& _v) const;

    /**
     * @brief Evaluate the cross product between this and another vectors.
     * @snippet Vector3D.cpp Vector3D::Cross
     *
     * @param _v Right hand vector
     * @return Vector3D resulted from the cross product
     */
    inline Vector3D operator^(const Vector3D& _v) const;

    /**
     * @brief Access the component by index. X=0, Y=1, Z=2
     *
     * @param _index The index of the component.
     * @return The component related to that index.
     */
    inline float& operator[](uint32 _index);

    /**
     * @brief Access the component by index. X=0, Y=1, Z=2
     *
     * @param _index The index of the component.
     * @return The component related to that index.
     */
    inline float operator[](uint32 _index) const;

    /**
     * @brief Compares if two vectors' components are equal.
     *
     * @param _other The vector being compared to this.
     * @return true If all components are equal.
     * @return false If any component is different.
     */
    inline bool operator==(const Vector3D& _other) const;

    /**
     * @brief Compares if two vectors' components are different.
     *
     * @param _other The vector being compared to this.
     * @return true If any component is different.
     * @return false If all components are equal.
     */
    inline bool operator!=(const Vector3D& _other) const;

    /**
     * @brief Compares if two vectors' components are equal.
     *
     * @param _other The vector being compared to this.
     * @param _tolerance Tolerance of the comparing
     * @return true If all components are equal.
     * @return false If any component is different.
     */
    inline bool Equals(const Vector3D& _other, float _tolerance = 0.0001f) const;

    /**
     * @brief Evaluate the dot product between two vectors.
     * @details The dot between \f$u=(u_x,u_y,u_z)\f$ and \f$v=(v_x,v_y,v_z)\f$ is
     * \f$u*v=u_x*v_x+u_y*v_y+u_z*v_z\f$
     * @snippet Vector3D.cpp Vector3D::Dot
     *
     * @param _u Left hand vector
     * @param _v Right hand vector
     * @return dot product between u and v
     */
    inline static float Dot(const Vector3D& _u, const Vector3D& _v);

    /**
     * @brief Evaluate the cross product between two vectors.
     * @snippet Vector3D.cpp Vector3D::Cross
     *
     * @param _u Left hand vector
     * @param _v Right hand vector
     * @return Vector3D resulted from the cross product
     */
    inline static Vector3D Cross(const Vector3D& _u, const Vector3D& _v);

    /**
     * @brief Evaluate the squared length of this vector.
     * @details Returns the squared distance between this vector tip and the origin. The squared
     * length of \f$(x,y,z)\f$ is \f$x^2+y^2+z^2\f$.
     *
     * @return The squared length of this vector.
     */
    inline float LengthSquared() const;

    /**
     * @brief Evaluate the length of this vector.
     * @details Returns the distance between this vector tip and the origin. The length of
     * \f$(x,y,z)\f$ is \f$\sqrt{x^2+y^2+z^2}\f$.
     * @snippet Vector3D.cpp Vector3D::Length
     *
     * @return The length of this vector.
     */
    inline float Length() const;

    /**
     * @brief Normalize this vector.
     *
     * @return Reference of this vector normalized.
     */
    inline Vector3D& Normalize();

    /**
     * @brief Get a normalized copy of this vector.
     *
     * @return A normalized copy of this vector.
     */
    inline Vector3D Normalized() const;

    /**
     * @brief Verify if all vector's components are equal to zero.
     *
     * @return true if all components are zero.
     * @return false if any of the components are different of zero.
     */
    inline bool IsZero() const;

    /**
     * @brief Verify if all components are \f$0<=x<=1\f$.
     *
     * @return true if all components are \f$0<=x<=1\f$.
     * @return false if any components are different \f$0<=x<=1\f$.
     */
    inline bool IsUnit() const;

    /** Vector at (0, 0, 0) */
    static AE_CORE_API const Vector3D Zero;

    /** Vector at (1, 1, 1) */
    static AE_CORE_API const Vector3D One;

    /** Vector at (1, 0, 0) */
    static AE_CORE_API const Vector3D Right;

    /** Vector at (-1, 0, 0) */
    static AE_CORE_API const Vector3D Left;

    /** Vector at (0, 0, 1) */
    static AE_CORE_API const Vector3D Up;

    /** Vector at (0, 0, -1) */
    static AE_CORE_API const Vector3D Down;

    /** Vector at (0, 1, 0) */
    static AE_CORE_API const Vector3D Forward;

    /** Vector at (0,-1, 0) */
    static AE_CORE_API const Vector3D Backward;
};

inline Vector3D::Vector3D()
  : X()
  , Y()
  , Z()
{}

inline Vector3D::Vector3D(float _xyz)
  : X(_xyz)
  , Y(_xyz)
  , Z(_xyz)
{}

inline Vector3D::Vector3D(float _x, float _y, float _z)
  : X(_x)
  , Y(_y)
  , Z(_z)
{}

inline Vector3D::Vector3D(const Vector3D& _other)
  : X(_other.X)
  , Y(_other.Y)
  , Z(_other.Z)
{}

inline Vector3D
Vector3D::operator-() const
{
    return Vector3D(-X, -Y, -Z);
}

inline Vector3D
Vector3D::operator+(float _s) const
{
    return Vector3D(X + _s, Y + _s, Z + _s);
}

inline Vector3D&
Vector3D::operator+=(float _s)
{
    X += _s;
    Y += _s;
    Z += _s;
    return *this;
}

inline Vector3D
Vector3D::operator+(const Vector3D& _v) const
{
    return Vector3D(X + _v.X, Y + _v.Y, Z + _v.Z);
}

inline Vector3D&
Vector3D::operator+=(const Vector3D& _v)
{
    X += _v.X;
    Y += _v.Y;
    Z += _v.Z;
    return *this;
}

inline Vector3D
Vector3D::operator-(float _s) const
{
    return Vector3D(X - _s, Y - _s, Z - _s);
}

inline Vector3D&
Vector3D::operator-=(float _s)
{
    X -= _s;
    Y -= _s;
    Z -= _s;
    return *this;
}

inline Vector3D
Vector3D::operator-(const Vector3D& _v) const
{
    return Vector3D(X - _v.X, Y - _v.Y, Z - _v.Z);
}

inline Vector3D&
Vector3D::operator-=(const Vector3D& _v)
{
    X -= _v.X;
    Y -= _v.Y;
    Z -= _v.Z;
    return *this;
}

inline Vector3D Vector3D::operator*(float _s) const { return Vector3D(X * _s, Y * _s, Z * _s); }

inline Vector3D&
Vector3D::operator*=(float _s)
{
    X *= _s;
    Y *= _s;
    Z *= _s;
    return *this;
}

inline Vector3D Vector3D::operator*(const Vector3D& _v) const
{
    return Vector3D(X * _v.X, Y * _v.Y, Z * _v.Z);
}

inline Vector3D&
Vector3D::operator*=(const Vector3D& _v)
{
    X *= _v.X;
    Y *= _v.Y;
    Z *= _v.Z;
    return *this;
}

inline Vector3D
Vector3D::operator/(float _s) const
{
    float invScalar = 1.0f / _s;
    return Vector3D(X * invScalar, Y * invScalar, Z * invScalar);
}

inline Vector3D&
Vector3D::operator/=(float _s)
{
    float invScalar = 1.0f / _s;
    X *= invScalar;
    Y *= invScalar;
    Z *= invScalar;
    return *this;
}

inline Vector3D
Vector3D::operator/(const Vector3D& _v) const
{
    return Vector3D(X / _v.X, Y / _v.Y, Z / _v.Z);
}

inline Vector3D&
Vector3D::operator/=(const Vector3D& _v)
{
    X /= _v.X;
    Y /= _v.Y;
    Z /= _v.Z;
    return *this;
}

inline float
Vector3D::LengthSquared() const
{
    return X * X + Y * Y + Z * Z;
}

inline float
Vector3D::Length() const
{
    return sqrtf(LengthSquared());
}

inline float
Vector3D::operator|(const Vector3D& _v) const
{
    return X * _v.X + Y * _v.Y + Z * _v.Z;
}

inline float
Vector3D::Dot(const Vector3D& _u, const Vector3D& _v)
{
    return _u | _v;
}

inline Vector3D
Vector3D::operator^(const Vector3D& _v) const
{
    return Vector3D(Y * _v.Z - Z * _v.Y, Z * _v.X - X * _v.Z, X * _v.Y - Y * _v.X);
}

inline float& Vector3D::operator[](uint32 _index)
{
    AE_ASSERT(_index < 3);
    return (&X)[_index];
}

inline float Vector3D::operator[](uint32 _index) const
{
    AE_ASSERT(_index < 3);
    return (&X)[_index];
}

inline bool
Vector3D::operator==(const Vector3D& _other) const
{
    return X == _other.X && Y == _other.Y && Z == _other.Z;
}

inline bool
Vector3D::operator!=(const Vector3D& _other) const
{
    return X != _other.X || Y != _other.Y || Z != _other.Z;
}

inline bool
Vector3D::Equals(const Vector3D& _other, float _tolerance) const
{
    return fabsf(X - _other.X) <= _tolerance && fabsf(Y - _other.Y) <= _tolerance
           && fabsf(Z - _other.Z) <= _tolerance;
}

inline Vector3D
Vector3D::Cross(const Vector3D& _u, const Vector3D& _v)
{
    return _u ^ _v;
}

inline Vector3D
Vector3D::Normalized() const
{
    AE_ASSERT(!IsZero());
    float length = 1.0f / Length();
    return Vector3D(X * length, Y * length, Z * length);
}

inline bool
Vector3D::IsZero() const
{
    return X == 0 && Y == 0 && Z == 0;
}

inline bool
Vector3D::IsUnit() const
{
    return (1.0f - LengthSquared()) >= 0.0f;
}

inline Vector3D&
Vector3D::Normalize()
{
    AE_ASSERT(!IsZero());
    float length = 1.0f / Length();
    X *= length;
    Y *= length;
    Z *= length;
    return *this;
}

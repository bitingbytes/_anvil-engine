#pragma once
/**
 * @file Vector4D.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2019-10-28
 *
 * @copyright Copyright (c) 2019
 *
 */

#include "CorePCH.pch.h"
#include "Types.h"
#include "Assertion.h"

/**
 * @brief Data representation of a 4D vector.
 *
 */
struct Vector4D
{
    /** Vector's X component. */
    float X;

    /** Vector's Y component. */
    float Y;

    /** Vector's Z component. */
    float Z;

    /** Vector's W component. */
    float W;

    /**
     * @brief Default constructor without initialization.
     *
     */
    inline Vector4D();

    /**
     * @brief Construct a Vector 4D initializing all components to a single scalar value.
     *
     * @param _xyzw Scalar to set all components to.
     */
    inline explicit Vector4D(float _xyzw);

    /**
     * @brief Construct a Vector 4D object setting all components.
     *
     * @param _x Sets the X component.
     * @param _y Sets the Y component.
     * @param _z Sets the Z component.
     * @param _w Sets the W component.
     */
    inline Vector4D(float _x, float _y, float _z, float _w = 1.0f);

    /**
     * @brief Construct a Vector 4D by coping another one.
     *
     * @param _other Vector to copy from.
     */
    inline Vector4D(const Vector4D& _other);

    /**
     * @brief Return a negated copy of this vector.
     *
     * @return a negated copy of this vector.
     */
    inline Vector4D operator-() const;

    /**
     * @brief Returns the addition between this vector's components and a scalar.
     *
     * @param _s Scalar to add to this.
     * @return Vector4D resulted from the addition.
     */
    inline Vector4D operator+(float _s) const;

    /**
     * @brief Returns the addition between this vector's components and a scalar.
     *
     * @param _s Scalar to add to this.
     * @return This Vector4D with resulted from the addition.
     */
    inline Vector4D& operator+=(float _s);

    /**
     * @brief Returns the addition between this vector's components and other vector's components.
     *
     * @param _v Vector to add to this.
     * @return Vector4D resulted from the addition.
     */
    inline Vector4D operator+(const Vector4D& _v) const;

    /**
     * @brief Returns the addition between this vector's components and a scalar.
     *
     * @param _v Scalar to add to this.
     * @return This Vector4D with resulted from the addition.
     */
    inline Vector4D& operator+=(const Vector4D& _v);

    /**
     * @brief Returns the subtraction between this vector's components and a scalar.
     *
     * @param _s Scalar to add to this.
     * @return Vector4D resulted from the subtraction.
     */
    inline Vector4D operator-(float _s) const;

    /**
     * @brief Returns the subtraction between this vector's components and a scalar.
     *
     * @param _s Scalar to add to this.
     * @return This Vector4D with resulted from the subtraction.
     */
    inline Vector4D& operator-=(float _s);

    /**
     * @brief Returns the subtraction between this vector's components and other vector's
     * components.
     *
     * @param _v Vector to add to this.
     * @return Vector4D resulted from the subtraction.
     */
    inline Vector4D operator-(const Vector4D& _v) const;

    /**
     * @brief Returns the subtraction between this vector's components and a scalar.
     *
     * @param _v Scalar to add to this.
     * @return This Vector4D with resulted from the subtraction.
     */
    inline Vector4D& operator-=(const Vector4D& _v);

    /**
     * @brief Returns the multiplication between this vector's components and a scalar.
     *
     * @param _s Scalar to add to this.
     * @return Vector4D resulted from the multiplication.
     */
    inline Vector4D operator*(float _s) const;

    /**
     * @brief Returns the multiplication between this vector's components and a scalar.
     *
     * @param _s Scalar to add to this.
     * @return This Vector4D with resulted from the multiplication.
     */
    inline Vector4D& operator*=(float _s);

    /**
     * @brief Returns the multiplication between this vector's components and other vector's
     * components.
     *
     * @param _v Vector to add to this.
     * @return Vector4D resulted from the multiplication.
     */
    inline Vector4D operator*(const Vector4D& _v) const;

    /**
     * @brief Returns the multiplication between this vector's components and a scalar.
     *
     * @param _v Scalar to add to this.
     * @return This Vector4D with resulted from the multiplication.
     */
    inline Vector4D& operator*=(const Vector4D& _v);

    /**
     * @brief Returns the division between this vector's components and a scalar.
     *
     * @param _s Scalar to add to this.
     * @return Vector4D resulted from the division.
     */
    inline Vector4D operator/(float _s) const;

    /**
     * @brief Returns the division between this vector's components and a scalar.
     *
     * @param _s Scalar to add to this.
     * @return This Vector4D with resulted from the division.
     */
    inline Vector4D& operator/=(float _s);

    /**
     * @brief Returns the division between this vector's components and other vector's components.
     *
     * @param _v Vector to add to this.
     * @return Vector4D resulted from the division.
     */
    inline Vector4D operator/(const Vector4D& _v) const;

    /**
     * @brief Returns the division between this vector's components and a scalar.
     *
     * @param _v Scalar to add to this.
     * @return This Vector4D with resulted from the division.
     */
    inline Vector4D& operator/=(const Vector4D& _v);

    /**
     * @brief Evaluate the dot product between this and another vectors.
     * @details The dot between \f$u=(u_x,u_y,u_z,u_w)\f$ and \f$v=(v_x,v_y,v_z,v_w)\f$ is
     * \f$u*v=u_x*v_x+u_y*v_y+u_z*v_z+u_w*v_w\f$
     * @snippet Vector4D.cpp Vector4D::Dot
     *
     * @param _v Right hand vector
     * @return Dot product between u and v
     */
    inline float operator|(const Vector4D& _v) const;

    /**
     * @brief Access the component by index. X=0, Y=1, Z=2, W=3
     *
     * @param _index The index of the component.
     * @return The component related to that index.
     */
    inline float& operator[](uint32 _index);

    /**
     * @brief Access the component by index. X=0, Y=1, Z=2, W=3
     *
     * @param _index The index of the component.
     * @return The component related to that index.
     */
    inline float operator[](uint32 _index) const;

    /**
     * @brief Compares if two vectors' components are equal.
     *
     * @param _other The vector being compared to this.
     * @return true If all components are equal.
     * @return false If any component is different.
     */
    inline bool operator==(const Vector4D& _other) const;

    /**
     * @brief Compares if two vectors' components are different.
     *
     * @param _other The vector being compared to this.
     * @return true If any component is different.
     * @return false If all components are equal.
     */
    inline bool operator!=(const Vector4D& _other) const;

    /**
     * @brief Compares if two vectors' components are equal.
     *
     * @param _other The vector being compared to this.
     * @param _tolerance Tolerance of the comparing
     * @return true If all components are equal.
     * @return false If any component is different.
     */
    inline bool Equals(const Vector4D& _other, float _tolerance = 0.0001f) const;

    /**
     * @brief Evaluate the dot product between two vectors.
     * @details The dot between \f$u=(u_x,u_y,u_z,u_w)\f$ and \f$v=(v_x,v_y,v_z,v_w)\f$ is
     * \f$u*v=u_x*v_x+u_y*v_y+u_z*v_z+u_w*v_w\f$
     * @snippet Vector4D.cpp Vector4D::Dot
     *
     * @param _u Left hand vector
     * @param _v Right hand vector
     * @return dot product between u and v
     */
    inline static float Dot(const Vector4D& _u, const Vector4D& _v);

    /**
     * @brief Evaluate the squared length of this vector.
     * @details Returns the squared distance between this vector tip and the origin. The squared
     * length of \f$(x,y,z,w)\f$ is \f$x^2+y^2+z^2+w^2\f$.
     *
     * @return The squared length of this vector.
     */
    inline float LengthSquared() const;

    /**
     * @brief Evaluate the length of this vector.
     * @details Returns the distance between this vector tip and the origin. The length of
     * \f$(x,y,z,w)\f$ is \f$\sqrt{x^2+y^2+z^2+w^2}\f$.
     * @snippet Vector4D.cpp Vector4D::Length
     *
     * @return The length of this vector.
     */
    inline float Length() const;

    /**
     * @brief Normalize this vector.
     *
     * @return Reference of this vector normalized.
     */
    inline Vector4D& Normalize();

    /**
     * @brief Get a normalized copy of this vector.
     *
     * @return A normalized copy of this vector.
     */
    inline Vector4D Normalized() const;

    /**
     * @brief Verify if all vector's components are equal to zero.
     *
     * @return true if all components are zero.
     * @return false if any of the components are different of zero.
     */
    inline bool IsZero() const;

    /**
     * @brief Verify if all components are \f$0<=x<=1\f$.
     *
     * @return true if all components are \f$0<=x<=1\f$.
     * @return false if any components are different \f$0<=x<=1\f$.
     */
    inline bool IsUnit() const;

    /** Vector at (0, 0, 0, 0) */
    static AE_CORE_API const Vector4D Zero;

    /** Vector at (1, 1, 1, 1) */
    static AE_CORE_API const Vector4D One;

    /** Vector at (1, 0, 0, 0) */
    static AE_CORE_API const Vector4D Right;

    /** Vector at (-1, 0, 0, 0) */
    static AE_CORE_API const Vector4D Left;

    /** Vector at (0, 0, 1, 0) */
    static AE_CORE_API const Vector4D Up;

    /** Vector at (0, 0, -1, 0) */
    static AE_CORE_API const Vector4D Down;

    /** Vector at (0, 1, 0, 0) */
    static AE_CORE_API const Vector4D Forward;

    /** Vector at (0,-1, 0, 0) */
    static AE_CORE_API const Vector4D Backward;
};

inline Vector4D::Vector4D()
  : X()
  , Y()
  , Z()
  , W()
{}

inline Vector4D::Vector4D(float _xyzw)
  : X(_xyzw)
  , Y(_xyzw)
  , Z(_xyzw)
  , W(_xyzw)
{}

inline Vector4D::Vector4D(float _x, float _y, float _z, float _w)
  : X(_x)
  , Y(_y)
  , Z(_z)
  , W(_w)
{}

inline Vector4D::Vector4D(const Vector4D& _other)
  : X(_other.X)
  , Y(_other.Y)
  , Z(_other.Z)
  , W(_other.W)
{}

inline Vector4D
Vector4D::operator-() const
{
    return Vector4D(-X, -Y, -Z, -W);
}

inline Vector4D
Vector4D::operator+(float _s) const
{
    return Vector4D(X + _s, Y + _s, Z + _s, W + _s);
}

inline Vector4D&
Vector4D::operator+=(float _s)
{
    X += _s;
    Y += _s;
    Z += _s;
    W += _s;
    return *this;
}

inline Vector4D
Vector4D::operator+(const Vector4D& _v) const
{
    return Vector4D(X + _v.X, Y + _v.Y, Z + _v.Z, W + _v.W);
}

inline Vector4D&
Vector4D::operator+=(const Vector4D& _v)
{
    X += _v.X;
    Y += _v.Y;
    Z += _v.Z;
    W += _v.W;
    return *this;
}

inline Vector4D
Vector4D::operator-(float _s) const
{
    return Vector4D(X - _s, Y - _s, Z - _s, W - _s);
}

inline Vector4D&
Vector4D::operator-=(float _s)
{
    X -= _s;
    Y -= _s;
    Z -= _s;
    W -= _s;
    return *this;
}

inline Vector4D
Vector4D::operator-(const Vector4D& _v) const
{
    return Vector4D(X - _v.X, Y - _v.Y, Z - _v.Z, W - _v.W);
}

inline Vector4D&
Vector4D::operator-=(const Vector4D& _v)
{
    X -= _v.X;
    Y -= _v.Y;
    Z -= _v.Z;
    W -= _v.W;
    return *this;
}

inline Vector4D Vector4D::operator*(float _s) const
{
    return Vector4D(X * _s, Y * _s, Z * _s, W * _s);
}

inline Vector4D&
Vector4D::operator*=(float _s)
{
    X *= _s;
    Y *= _s;
    Z *= _s;
    W *= _s;
    return *this;
}

inline Vector4D Vector4D::operator*(const Vector4D& _v) const
{
    return Vector4D(X * _v.X, Y * _v.Y, Z * _v.Z, Z * _v.Z);
}

inline Vector4D&
Vector4D::operator*=(const Vector4D& _v)
{
    X *= _v.X;
    Y *= _v.Y;
    Z *= _v.Z;
    W *= _v.W;
    return *this;
}

inline Vector4D
Vector4D::operator/(float _s) const
{
    float invScalar = 1.0f / _s;
    return Vector4D(X * invScalar, Y * invScalar, Z * invScalar, W * invScalar);
}

inline Vector4D&
Vector4D::operator/=(float _s)
{
    float invScalar = 1.0f / _s;
    X *= invScalar;
    Y *= invScalar;
    Z *= invScalar;
    W *= invScalar;
    return *this;
}

inline Vector4D
Vector4D::operator/(const Vector4D& _v) const
{
    return Vector4D(X / _v.X, Y / _v.Y, Z / _v.Z, W / _v.W);
}

inline Vector4D&
Vector4D::operator/=(const Vector4D& _v)
{
    X /= _v.X;
    Y /= _v.Y;
    Z /= _v.Z;
    W /= _v.W;
    return *this;
}

inline float
Vector4D::LengthSquared() const
{
    return X * X + Y * Y + Z * Z + W * W;
}

inline float
Vector4D::Length() const
{
    return sqrtf(LengthSquared());
}

inline float
Vector4D::operator|(const Vector4D& _v) const
{
    return X * _v.X + Y * _v.Y + Z * _v.Z + W * _v.W;
}

inline float
Vector4D::Dot(const Vector4D& _u, const Vector4D& _v)
{
    return _u | _v;
}

inline float& Vector4D::operator[](uint32 _index)
{
    AE_ASSERT(_index < 4);
    return (&X)[_index];
}

inline float Vector4D::operator[](uint32 _index) const
{
    AE_ASSERT(_index < 4);
    return (&X)[_index];
}

inline bool
Vector4D::operator==(const Vector4D& _other) const
{
    return X == _other.X && Y == _other.Y && Z == _other.Z && W == _other.W;
}

inline bool
Vector4D::operator!=(const Vector4D& _other) const
{
    return X != _other.X || Y != _other.Y || Z != _other.Z || W != _other.W;
}

inline bool
Vector4D::Equals(const Vector4D& _other, float _tolerance) const
{
    return fabsf(X - _other.X) <= _tolerance && fabsf(Y - _other.Y) <= _tolerance
           && fabsf(Z - _other.Z) <= _tolerance && fabsf(W - _other.W) <= _tolerance;
}

inline Vector4D
Vector4D::Normalized() const
{
    AE_ASSERT(!IsZero());
    float length = 1.0f / Length();
    return Vector4D(X * length, Y * length, Z * length, W * length);
}

inline bool
Vector4D::IsZero() const
{
    return X == 0 && Y == 0 && Z == 0 && W == 0;
}

inline bool
Vector4D::IsUnit() const
{
    return (1.0f - LengthSquared()) >= 0.0f;
}

inline Vector4D&
Vector4D::Normalize()
{
    AE_ASSERT(!IsZero());
    float length = 1.0f / Length();
    X *= length;
    Y *= length;
    Z *= length;
    W *= length;
    return *this;
}

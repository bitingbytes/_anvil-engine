#pragma once
/**
 * @file Vector2D.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2019-10-28
 *
 * @copyright Copyright (c) 2019
 *
 */

#include "CorePCH.pch.h"
#include "Types.h"
#include "Assertion.h"

/**
 * @brief Data representation of a 2D vector.
 *
 */
struct Vector2D
{
    /** Vector's X component. */
    float X;

    /** Vector's Y component. */
    float Y;

    /**
     * @brief Default constructor without initialization.
     *
     */
    inline Vector2D();

    /**
     * @brief Construct a Vector 2D initializing all components to a single scalar value.
     *
     * @param _xy Scalar to set all components to.
     */
    inline explicit Vector2D(float _xy);

    /**
     * @brief Construct a Vector 2D object setting all components.
     *
     * @param _x Sets the X component.
     * @param _y Sets the Y component.
     */
    inline Vector2D(float _x, float _y);

    /**
     * @brief Construct a Vector 2D by coping another one.
     *
     * @param _other Vector to copy from.
     */
    inline Vector2D(const Vector2D& _other);

    /**
     * @brief Return a negated copy of this vector.
     *
     * @return a negated copy of this vector.
     */
    inline Vector2D operator-() const;

    /**
     * @brief Returns the addition between this vector's components and a scalar.
     *
     * @param _s Scalar to add to this.
     * @return Vector2D resulted from the addition.
     */
    inline Vector2D operator+(float _s) const;

    /**
     * @brief Returns the addition between this vector's components and a scalar.
     *
     * @param _s Scalar to add to this.
     * @return This Vector2D with resulted from the addition.
     */
    inline Vector2D& operator+=(float _s);

    /**
     * @brief Returns the addition between this vector's components and other vector's components.
     *
     * @param _v Vector to add to this.
     * @return Vector2D resulted from the addition.
     */
    inline Vector2D operator+(const Vector2D& _v) const;

    /**
     * @brief Returns the addition between this vector's components and a scalar.
     *
     * @param _v Scalar to add to this.
     * @return This Vector2D with resulted from the addition.
     */
    inline Vector2D& operator+=(const Vector2D& _v);

    /**
     * @brief Returns the subtraction between this vector's components and a scalar.
     *
     * @param _s Scalar to add to this.
     * @return Vector2D resulted from the subtraction.
     */
    inline Vector2D operator-(float _s) const;

    /**
     * @brief Returns the subtraction between this vector's components and a scalar.
     *
     * @param _s Scalar to add to this.
     * @return This Vector2D with resulted from the subtraction.
     */
    inline Vector2D& operator-=(float _s);

    /**
     * @brief Returns the subtraction between this vector's components and other vector's
     * components.
     *
     * @param _v Vector to add to this.
     * @return Vector2D resulted from the subtraction.
     */
    inline Vector2D operator-(const Vector2D& _v) const;

    /**
     * @brief Returns the subtraction between this vector's components and a scalar.
     *
     * @param _v Scalar to add to this.
     * @return This Vector2D with resulted from the subtraction.
     */
    inline Vector2D& operator-=(const Vector2D& _v);

    /**
     * @brief Returns the multiplication between this vector's components and a scalar.
     *
     * @param _s Scalar to add to this.
     * @return Vector2D resulted from the multiplication.
     */
    inline Vector2D operator*(float _s) const;

    /**
     * @brief Returns the multiplication between this vector's components and a scalar.
     *
     * @param _s Scalar to add to this.
     * @return This Vector2D with resulted from the multiplication.
     */
    inline Vector2D& operator*=(float _s);

    /**
     * @brief Returns the multiplication between this vector's components and other vector's
     * components.
     *
     * @param _v Vector to add to this.
     * @return Vector2D resulted from the multiplication.
     */
    inline Vector2D operator*(const Vector2D& _v) const;

    /**
     * @brief Returns the multiplication between this vector's components and a scalar.
     *
     * @param _v Scalar to add to this.
     * @return This Vector2D with resulted from the multiplication.
     */
    inline Vector2D& operator*=(const Vector2D& _v);

    /**
     * @brief Returns the division between this vector's components and a scalar.
     *
     * @param _s Scalar to add to this.
     * @return Vector2D resulted from the division.
     */
    inline Vector2D operator/(float _s) const;

    /**
     * @brief Returns the division between this vector's components and a scalar.
     *
     * @param _s Scalar to add to this.
     * @return This Vector2D with resulted from the division.
     */
    inline Vector2D& operator/=(float _s);

    /**
     * @brief Returns the division between this vector's components and other vector's components.
     *
     * @param _v Vector to add to this.
     * @return Vector2D resulted from the division.
     */
    inline Vector2D operator/(const Vector2D& _v) const;

    /**
     * @brief Returns the division between this vector's components and a scalar.
     *
     * @param _v Scalar to add to this.
     * @return This Vector2D with resulted from the division.
     */
    inline Vector2D& operator/=(const Vector2D& _v);

    /**
     * @brief Evaluate the dot product between this and another vectors.
     * @details The dot between \f$u=(u_x,u_y)\f$ and \f$v=(v_x,v_y)\f$ is
     * \f$u*v=u_x*v_x+u_y*v_y\f$
     * @snippet Vector2D.cpp Vector2D::Dot
     *
     * @param _v Right hand vector
     * @return Dot product between u and v
     */
    inline float operator|(const Vector2D& _v) const;

    /**
     * @brief Access the component by index. X=0, Y=1
     *
     * @param _index The index of the component.
     * @return The component related to that index.
     */
    inline float& operator[](uint32 _index);

    /**
     * @brief Compares if two vectors' components are equal.
     *
     * @param _other The vector being compared to this.
     * @return true If all components are equal.
     * @return false If any component is different.
     */
    inline bool operator==(const Vector2D& _other) const;

    /**
     * @brief Compares if two vectors' components are different.
     *
     * @param _other The vector being compared to this.
     * @return true If any component is different.
     * @return false If all components are equal.
     */
    inline bool operator!=(const Vector2D& _other) const;

    /**
     * @brief Compares if two vectors' components are equal.
     *
     * @param _other The vector being compared to this.
     * @param _tolerance Tolerance of the comparing
     * @return true If all components are equal.
     * @return false If any component is different.
     */
    inline bool Equals(const Vector2D& _other, float _tolerance = 0.0001f) const;

    /**
     * @brief Access the component by index. X=0, Y=1
     *
     * @param _index The index of the component.
     * @return The component related to that index.
     */
    inline float operator[](uint32 _index) const;

    /**
     * @brief Evaluate the dot product between two vectors.
     * @details The dot between \f$u=(u_x,u_y)\f$ and \f$v=(v_x,v_y)\f$ is
     * \f$u*v=u_x*v_x+u_y*v_y\f$
     * @snippet Vector2D.cpp Vector2D::Dot
     *
     * @param _u Left hand vector
     * @param _v Right hand vector
     * @return dot product between u and v
     */
    inline static float Dot(const Vector2D& _u, const Vector2D& _v);

    /**
     * @brief Evaluate the squared length of this vector.
     * @details Returns the squared distance between this vector tip and the origin. The squared
     * length of \f$(x,y)\f$ is \f$x^2+y^2\f$.
     *
     * @return The squared length of this vector.
     */
    inline float LengthSquared() const;

    /**
     * @brief Evaluate the length of this vector.
     * @details Returns the distance between this vector tip and the origin. The length of
     * \f$(x,y)\f$ is \f$\sqrt{x^2+y^2}\f$.
     * @snippet Vector2D.cpp Vector2D::Length
     *
     * @return The length of this vector.
     */
    inline float Length() const;

    /**
     * @brief Normalize this vector.
     *
     * @return Reference of this vector normalized.
     */
    inline Vector2D& Normalize();

    /**
     * @brief Get a normalized copy of this vector.
     *
     * @return A normalized copy of this vector.
     */
    inline Vector2D Normalized() const;

    /**
     * @brief Verify if all vector's components are equal to zero.
     *
     * @return true if all components are zero.
     * @return false if any of the components are different of zero.
     */
    inline bool IsZero() const;

    /**
     * @brief Verify if all components are \f$0<=x<=1\f$.
     *
     * @return true if all components are \f$0<=x<=1\f$.
     * @return false if any components are different \f$0<=x<=1\f$.
     */
    inline bool IsUnit() const;

    /** Vector at (0, 0) */
    static AE_CORE_API const Vector2D Zero;

    /** Vector at (1, 1) */
    static AE_CORE_API const Vector2D One;

    /** Vector at (1, 0) */
    static AE_CORE_API const Vector2D Right;

    /** Vector at (-1, 0) */
    static AE_CORE_API const Vector2D Left;

    /** Vector at (0, 1) */
    static AE_CORE_API const Vector2D Up;

    /** Vector at (0, -1) */
    static AE_CORE_API const Vector2D Down;
};

inline Vector2D::Vector2D()
  : X()
  , Y()
{}

inline Vector2D::Vector2D(float _xy)
  : X(_xy)
  , Y(_xy)
{}

inline Vector2D::Vector2D(float _x, float _y)
  : X(_x)
  , Y(_y)
{}

inline Vector2D::Vector2D(const Vector2D& _other)
  : X(_other.X)
  , Y(_other.Y)
{}

inline Vector2D
Vector2D::operator-() const
{
    return Vector2D(-X, -Y);
}

inline Vector2D
Vector2D::operator+(float _s) const
{
    return Vector2D(X + _s, Y + _s);
}

inline Vector2D&
Vector2D::operator+=(float _s)
{
    X += _s;
    Y += _s;
    return *this;
}

inline Vector2D
Vector2D::operator+(const Vector2D& _v) const
{
    return Vector2D(X + _v.X, Y + _v.Y);
}

inline Vector2D&
Vector2D::operator+=(const Vector2D& _v)
{
    X += _v.X;
    Y += _v.Y;
    return *this;
}

inline Vector2D
Vector2D::operator-(float _s) const
{
    return Vector2D(X - _s, Y - _s);
}

inline Vector2D&
Vector2D::operator-=(float _s)
{
    X -= _s;
    Y -= _s;
    return *this;
}

inline Vector2D
Vector2D::operator-(const Vector2D& _v) const
{
    return Vector2D(X - _v.X, Y - _v.Y);
}

inline Vector2D&
Vector2D::operator-=(const Vector2D& _v)
{
    X -= _v.X;
    Y -= _v.Y;
    return *this;
}

inline Vector2D Vector2D::operator*(float _s) const { return Vector2D(X * _s, Y * _s); }

inline Vector2D&
Vector2D::operator*=(float _s)
{
    X *= _s;
    Y *= _s;
    return *this;
}

inline Vector2D Vector2D::operator*(const Vector2D& _v) const
{
    return Vector2D(X * _v.X, Y * _v.Y);
}

inline Vector2D&
Vector2D::operator*=(const Vector2D& _v)
{
    X *= _v.X;
    Y *= _v.Y;
    return *this;
}

inline Vector2D
Vector2D::operator/(float _s) const
{
    float invScalar = 1.0f / _s;
    return Vector2D(X * invScalar, Y * invScalar);
}

inline Vector2D&
Vector2D::operator/=(float _s)
{
    float invScalar = 1.0f / _s;
    X *= invScalar;
    Y *= invScalar;
    return *this;
}

inline Vector2D
Vector2D::operator/(const Vector2D& _v) const
{
    return Vector2D(X / _v.X, Y / _v.Y);
}

inline Vector2D&
Vector2D::operator/=(const Vector2D& _v)
{
    X /= _v.X;
    Y /= _v.Y;
    return *this;
}

inline float
Vector2D::LengthSquared() const
{
    return X * X + Y * Y;
}

inline float
Vector2D::Length() const
{
    return sqrtf(LengthSquared());
}

inline float
Vector2D::operator|(const Vector2D& _v) const
{
    return X * _v.X + Y * _v.Y;
}

inline float
Vector2D::Dot(const Vector2D& _u, const Vector2D& _v)
{
    return _u | _v;
}

inline float& Vector2D::operator[](uint32 _index)
{
    AE_ASSERT(_index < 2);
    return (&X)[_index];
}

inline bool
Vector2D::operator==(const Vector2D& _other) const
{
    return X == _other.X && Y == _other.Y;
}

inline bool
Vector2D::operator!=(const Vector2D& _other) const
{
    return X != _other.X || Y != _other.Y;
}

inline bool
Vector2D::Equals(const Vector2D& _other, float _tolerance) const
{
    return fabsf(X - _other.X) <= _tolerance && fabsf(Y - _other.Y) <= _tolerance;
}

inline float Vector2D::operator[](uint32 _index) const
{
    AE_ASSERT(_index < 2);
    return (&X)[_index];
}

inline Vector2D
Vector2D::Normalized() const
{
    AE_ASSERT(!IsZero());
    float length = 1.0f / Length();
    return Vector2D(X * length, Y * length);
}

inline bool
Vector2D::IsZero() const
{
    return X == 0 && Y == 0;
}

inline bool
Vector2D::IsUnit() const
{
    return (1.0f - LengthSquared()) >= 0.0f;
}

inline Vector2D&
Vector2D::Normalize()
{
    AE_ASSERT(!IsZero());
    float length = 1.0f / Length();
    X *= length;
    Y *= length;
    return *this;
}

/**
 * @file Color.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2019-11-11
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once

#include "CorePCH.pch.h"
#include "Types.h"

struct Color
{
    float R;
    float G;
    float B;
    float A;

    finline Color();

    finline explicit Color(float _rgba);

    finline Color(float _r, float _g, float _b, float _a = 1.0f);

    finline Color(const Color& _other);

    finline float* ToArray() { return reinterpret_cast<float*>(this); }

    static AE_CORE_API const Color Black;
    static AE_CORE_API const Color White;
    static AE_CORE_API const Color Red;
    static AE_CORE_API const Color Green;
    static AE_CORE_API const Color Blue;
    static AE_CORE_API const Color Yellow;
    static AE_CORE_API const Color Magenta;
    static AE_CORE_API const Color Cyan;
};

finline
Color::Color()
  : R()
  , G()
  , B()
  , A()
{}

finline
Color::Color(float _rgba)
  : R(_rgba)
  , G(_rgba)
  , B(_rgba)
  , A(_rgba)
{}

finline
Color::Color(float _r, float _g, float _b, float _a)
  : R(_r)
  , G(_g)
  , B(_b)
  , A(_a)
{}

finline
Color::Color(const Color& _other)
  : R(_other.R)
  , G(_other.G)
  , B(_other.B)
  , A(_other.A)
{}

/**
 * @file Logger.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2019-10-31
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once

#include "Containers/AString.h"
#include "Platform/FileSystem.h"
#include "Types.h"
#include "Utils/LibraryUtils.h"

enum class ELogLevel
{
    Fatal,
    Error,
    Warning,
    Info,
    Verbose,
    All
};

class AE_CORE_API Logger
{
  public:
    Logger(const String& _category, ELogLevel _level);

    void Log(ELogLevel _level, const tchar* _format, ...);

    String GetLogLevelName(ELogLevel _level) const;

    std::unique_ptr<FileHandle> m_logFile;

  private:
    String    m_category;
    ELogLevel m_level;
};

#define CREATE_LOGGER_EXTERN(category, level)         \
    extern struct Logger##category : public Logger    \
    {                                                 \
        finline Logger##category()                    \
          : Logger(TEXT(#category), ELogLevel::level) \
        {}                                            \
    } category

#define CREATE_LOGGER_STATIC(category, level)         \
    static struct Logger##category : public Logger    \
    {                                                 \
        finline Logger##category()                    \
          : Logger(TEXT(#category), ELogLevel::level) \
        {}                                            \
    } category

#define CREATE_LOGGER(category) Logger##category category

#define AE_FATAL(category, format, ...) category.Log(ELogLevel::Fatal, format, ##__VA_ARGS__)

#define AE_ERROR(category, format, ...) category.Log(ELogLevel::Error, format, ##__VA_ARGS__)

#define AE_WARN(category, format, ...) category.Log(ELogLevel::Warning, format, ##__VA_ARGS__)

#define AE_INFO(category, format, ...) category.Log(ELogLevel::Info, format, ##__VA_ARGS__)

#define AE_VERB(category, format, ...) category.Log(ELogLevel::Verbose, format, ##__VA_ARGS__)
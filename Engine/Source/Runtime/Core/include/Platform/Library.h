/**
 * @file Library.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2019-11-05
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once

#include "Containers/AString.h"

class AE_CORE_API Library
{
  public:
    /**
     * @brief Construct a new ALibrary object loading the file from _path.
     *
     * @param _path
     */
    Library(const String& _path);

    /**
     * @brief Destroy the ALibrary object.
     *
     * Don't forget to call Unload explicitly or the Library is going to be in memory until the
     * termination.
     *
     */
    ~Library();

    /**
     * @brief Get the last error.
     *
     */
    inline String GetError() const { return m_error; }

    /**
     * @brief Get the library path.
     *
     */
    inline String GetPath() const { return m_path; }

    /**
     * @brief Returns true if the library is loaded.
     *
     */
    inline bool IsLoaded() const { return m_bIsLoaded; }

    /**
     * @brief Loads the library and returns true if loaded sucessfuly.
     *
     */
    bool Load();

    /**
     * @brief Unloads library and releases the handle.
     *
     */
    bool Unload();

    /**
     * @brief Resolves symbol on library and retuns pointer to function address.
     *
     */
    void* Resolve(const tchar* _symbol);

    template<class T>
    finline T Resolve(const tchar* _symbol)
    {
        return reinterpret_cast<T>(Resolve(_symbol));
    }

    static bool IsLibrary(const std::string& _path);

  protected:
    String m_path;
    String m_error;
    uint8  m_bIsLoaded : 1;
    void*  m_pHandle;
};

using FModuleInitialize = void* (*)();

#ifndef AE_MONOLITHIC
#define LOAD_MODULE(x)                                            \
    Library           Module(#x);                                 \
    FModuleInitialize x##InitializeModule                         \
      = (FModuleInitialize)Module.Resolve(#x "InitializeModule"); \
    if (!x##InitializeModule) {                                   \
        return false;                                             \
    }
#else
#define LOAD_MODULE(x)
#endif // !AE_MONOLITHIC
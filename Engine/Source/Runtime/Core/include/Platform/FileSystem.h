/**
 * @file FileSystem.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2019-11-01
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once

#include "Assertion.h"
#include "Containers/AString.h"
#include "CorePCH.pch.h"
#include "Types.h"
#include "Utils/LibraryUtils.h"

/**
 * @brief Wrapper to platform file handle.
 *
 */
class AE_CORE_API FileHandle
{
  public:
    /**
     * @brief Destroy the File Handle, closing the file.
     *
     */
    virtual ~FileHandle() = default;

    /**
     * @brief Offsets the file cursor to a position.
     *
     * @param _pos The position to offsets.
     * @param _mode The mode of offset. SEEK_SET, SEEK_CUR, SEEK_END.
     */
    virtual void Seek(int64 _pos, int32 _mode) = 0;

    /**
     * @brief Flush the file stream.
     *
     */
    virtual void Flush() = 0;

    /**
     * @brief Closes the file.
     *
     */
    virtual void Close() = 0;

    /**
     * @brief Verifies if the file is valid.
     *
     * @return true When the file is valid.
     * @return false When the file is not valid.
     */
    virtual bool IsValid() const = 0;

    /**
     * @brief Get the Native Handle pointer.
     *
     * @return void* Native handle pointer.
     */
    virtual void* GetNativeHandle() = 0;

    /**
     * @brief Tells where the file cursor is.
     *
     * @return uint64 Position of the file cursor.
     */
    virtual uint64 Tell() const = 0;

    /**
     * @brief Get the Size of the file.
     *
     * @return uint64 Size of the file.
     */
    virtual uint64 GetSize() const = 0;

    /**
     * @brief Verifies if the cursos is at the end of file.
     *
     * @return true If the cursos is at the end of file.
     * @return false If the cursos is not at the end of file.
     */
    virtual bool IsEOF() const = 0;

    /**
     * @brief Verifies if the file is read only.
     *
     * @return true If the file is read only.
     * @return false If the file is not read only.
     */
    virtual bool IsReadOnly() const = 0;

    /**
     * @brief Verifies if the file is write only.
     *
     * @return true If the file is write only.
     * @return false If the file is not write only.
     */
    virtual bool IsWriteOnly() const = 0;

    /**
     * @brief Reads a sized buffer from the file.
     *
     * @param _buffer A pointer to the buffer being read.
     * @param _size The size to be read.
     * @return uint64 Bytes read.
     */
    virtual uint64 Read(void* _buffer, uint64 _size) = 0;

    /**
     * @brief Reads a non null-terminated string.
     *
     * @param _string Reference to the read string.
     * @return uint64 Size of the read string.
     */
    virtual uint64 ReadString(String& _string) = 0;

    /**
     * @brief Writes a buffer to a file.
     *
     * @param _buffer The buffer to be writed.
     * @param _size Size of the buffer being written.
     * @return uint64 Bytes wroten.
     */
    virtual uint64 Write(const void* _buffer, uint64 _size) = 0;

    /**
     * @brief Writes a non null-terminated string.
     *
     * @param _string String to be written.
     * @return uint64 Size of the written string.
     */
    virtual uint64 WriteString(const String& _string) = 0;

    /**
     * @brief Reads a typed buffer.
     *
     * @tparam T Type of the buffer.
     * @param _buffer Pointer to the buffer.
     * @return uint64 Bytes read.
     */
    template<class T>
    uint64 Read(T* _buffer)
    {
        return Read(_buffer, sizeof(T));
    }

    /**
     * @brief Writes a typed buffer.
     *
     * @tparam T Type of the buffer.
     * @param _buffer Pointer to the buffer
     * @return uint64 Bytes read.
     */
    template<class T>
    uint64 Write(const T* _buffer)
    {
        return Write(_buffer, sizeof(T));
    }

    template<class T>
    uint64 Write(const std::vector<T, std::allocator<T>>& _vector)
    {
        int32 size = (int32)_vector.size();
        Write(&size, sizeof(int32));
        return Write(_vector.data(), sizeof(T) * _vector.size());
    }

    FileHandle& operator<<(const String& _str)
    {
        AE_ASSERT(!IsReadOnly());
        Write(_str.data(), _str.size() * sizeof(String::value_type));
        return *this;
    }

    virtual uint64 CheckSum() = 0;
};

/**
 * @brief Wrapper to platform file system.
 *
 */
class AE_CORE_API FileSystem
{
  public:
    /**
     * @brief Get the Platform's File System.
     *
     * @return The platform's file system.
     */
    static FileSystem& GetPlatformFS();

    virtual ~FileSystem() = default;

    /**
     * @brief Verifies if a file exists.
     *
     * @param _path The file's path.
     * @return true If the file exists.
     * @return false If the file don't exists or if it is a directory.
     */
    virtual bool FileExists(const tchar* _path) = 0;

    /**
     * @brief Verifies if a file exists.
     *
     * @param _path The file's path.
     * @return true If the file exists.
     * @return false If the file don't exists or if it is a directory.
     */
    virtual bool DirectoryExists(const tchar* _path) = 0;

    virtual bool MakeDirectory(const tchar* _path) = 0;

    /**
     * @brief Sets the current working directory.
     *
     * @param _path The current working directory's path.
     * @return true If the current working directory was setted correctly.
     * @return false If an error occurried.
     */
    virtual bool SetCWD(const tchar* _path) = 0;

    /**
     * @brief Gets the current working directory.
     *
     * @return const tchar* the current working directory.
     */
    virtual String GetCWD() const = 0;

    virtual std::vector<String> GetAllFiles(const tchar* _path, const tchar* _pattern = "/*") = 0;

    /**
     * @brief Opens a file for reading.
     *
     * @param _path The path to the file.
     * @param _canWrite Enables writing.
     * @return FileHandle* A pointer that represents the file. Delete to close the file.
     */
    virtual FileHandle* OpenRead(const tchar* _path, bool _canWrite = false) = 0;

    /**
     * @brief Opens a file for writing.
     *
     * @param _path The path to the file.
     * @param _append Appends data.
     * @param _canRead Enables reading.
     * @return FileHandle* A pointer that represents the file. Delete to close the file.
     */
    virtual FileHandle* OpenWrite(const tchar* _path, bool _append = false, bool _canRead = false) = 0;
};
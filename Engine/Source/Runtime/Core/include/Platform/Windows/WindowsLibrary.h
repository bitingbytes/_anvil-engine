/**
 * @file WindowsLibrary.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2019-11-05
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once

#include "..\Library.h"
#include "WindowsIncludes.h"

Library::~Library() {}

inline bool
Library::Load()
{
    std::string Path = m_path + TEXT(".dll");
    m_pHandle        = LoadLibrary(Path.c_str());

    if (m_pHandle == nullptr) {
        return false;
    }

    m_bIsLoaded = true;
    return true;
}

inline bool
Library::Unload()
{
    if (!m_bIsLoaded) {
        return false;
    }

    BOOL Res = FreeLibrary(static_cast<HMODULE>(m_pHandle));
    if (Res == FALSE) {
        return false;
    }

    m_bIsLoaded = false;
    m_pHandle   = nullptr;

    return true;
}

inline void*
Library::Resolve(const tchar* _symbol)
{
    if (!m_bIsLoaded) {
        if (!Load()) {
            return nullptr;
        }
    }

    FARPROC Proc = GetProcAddress(static_cast<HMODULE>(m_pHandle), _symbol);
    if (Proc == nullptr) {
        m_error = TEXT("Could not resolve proc ");
        m_error += _symbol;
        return nullptr;
    }

    return Proc;
}

inline bool
Library::IsLibrary(const std::string& _path)
{
    String Path(_path);

    String FileExt = _path.substr(Path.find_last_of(TEXT('.')));

    if (strcmp(FileExt.c_str(), TEXT(".dll")) == 0) {
        return true;
    }

    if (strcmp(FileExt.c_str(), TEXT(".DLL")) == 0) {
        return true;
    }

    return false;
}
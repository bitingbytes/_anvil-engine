#pragma once

#ifndef _WINNT_
#undef TEXT
#endif
#ifndef STRICT
#define STRICT
#endif // !STRICT
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#define NOSOUND
#define NOCOMM
#define NOHELP
#define NOTEXT
#include <Windows.h>
#include <combaseapi.h>
#include <shellapi.h>
#undef UINT
#undef INT
#undef FLOAT
#undef DWORD
#undef CreateMetaFile

#define OUTPUT_CONSOLE(msg) OutputDebugString(msg)
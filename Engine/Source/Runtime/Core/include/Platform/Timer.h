/**
 * @file Timer.h
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2019-11-03
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once

/**
 * @brief High Resolution Timer
 *
 */
class HiResTimer
{
  public:
    /**
     * @brief Get the platform specific high resolution timer.
     *
     * @return HiResTimer& The reference platform specific high resolution timer.
     */
    static HiResTimer& GetPlatformHT();

    /**
     * @brief Get the Total Time passed since the game start.
     *
     * @return double The Total Time passed since the game start.
     */
    virtual double GetTotalTime() const = 0;

    /**
     * @brief Get the Delta Time.
     *
     * @return double The Delta Time.
     */
    virtual double GetDeltaTime() const = 0;

    virtual void Start() = 0;

    virtual void Stop() = 0;

    virtual void Reset() = 0;

    virtual void Tick() = 0;
};
#include "Containers/AEGUID.h"

AEGUID::AEGUID()
  : m_data1()
  , m_data2()
  , m_data3()
  , m_data4()
{}

AEGUID
AEGUID::Generate()
{
    AEGUID result;

#ifdef AE_WINDOWS
    GUID    guid;
    HRESULT hResult = CoCreateGuid(&guid);
    if (SUCCEEDED(hResult)) {
        result.m_data1 = guid.Data1;
        result.m_data2 = guid.Data2;
        result.m_data3 = guid.Data3;
        memcpy(result.m_data4, guid.Data4, sizeof(guid.Data4));
    }
#endif // AE_WINDOWS

    return result;
}

String
AEGUID::ToString(bool _useSeparator, bool _toUpper) const
{
    char str[37] = {0};

    int64 data4;
    memcpy(&data4, m_data4, sizeof(data4));

    if (_useSeparator) {
        snprintf(str, sizeof(str), "%02x-%02x-%02x-%02llx", m_data1, m_data2, m_data3, data4);
    } else {
        snprintf(str, sizeof(str), "%02x%02x%02x%02llx", m_data1, m_data2, m_data3, data4);
    }

    String result(str);

    if (_toUpper) {
        std::for_each(result.begin(), result.end(), [](char& c) { c = (char)std::toupper((int)c); });
    }

    return str;
}

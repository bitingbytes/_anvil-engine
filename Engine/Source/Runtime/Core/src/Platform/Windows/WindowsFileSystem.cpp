#include "Assertion.h"
#include "Containers/AString.h"
#include "Platform/FileSystem.h"
#include "Platform/Windows/WindowsIncludes.h"

class WindowsFileHandle : public FileHandle
{
  public:
    WindowsFileHandle(HANDLE _handle, bool _canRead, bool _canWrite)
      : m_handle(_handle)
      , m_overlapped({0})
      , m_pos(0)
      , m_size(0)
      , m_canRead(_canRead)
      , m_canWrite(_canWrite)
    {
        UpdateFileStats();
    }

    ~WindowsFileHandle() { Close(); }

    void Seek(int64 _pos, int32 _mode) final
    {
        AE_ASSERT(IsValid());

        switch (_mode) {
            case SEEK_SET:
                AE_ASSERT(_pos >= 0);
                m_pos = _pos;
                break;
            case SEEK_CUR:
                m_pos += _pos;
                break;
            case SEEK_END:
                AE_ASSERT(_pos <= 0);
                m_pos = m_size + _pos;
                break;
            default:
                break;
        }

        UpdateOverlapped();
    }

    void Flush() final
    {
        AE_ASSERT(IsValid());
        ::FlushFileBuffers(m_handle);
    }

    void Close() final
    {
        if (m_handle != nullptr) {
            ::CloseHandle(m_handle);
            m_handle = nullptr;
        }
    }

    bool IsValid() const final { return m_handle != INVALID_HANDLE_VALUE; }

    void* GetNativeHandle() final { return m_handle; }

    uint64 Tell() const final { return m_pos; }

    uint64 GetSize() const final { return m_size; }

    bool IsEOF() const final { return m_pos == m_size; }

    bool IsReadOnly() const final { return m_canRead && !m_canWrite; }

    bool IsWriteOnly() const final { return !m_canRead && m_canWrite; }

    uint64 Read(void* _buffer, uint64 _size) final
    {
        AE_ASSERT(IsValid());

        if (_size <= 0) {
            return 0;
        }

        DWORD BytesRead = 0;

        if (::ReadFile(m_handle, _buffer, static_cast<DWORD>(_size), &BytesRead, &m_overlapped)
            == FALSE) {
            auto Err = GetLastError();
            if (Err != ERROR_IO_PENDING) {
                return 0;
            }

            BytesRead = 0;
            if (GetOverlappedResult(m_handle, &m_overlapped, &BytesRead, TRUE) == FALSE) {
                Err = GetLastError();
                return 0;
            }
        }

        m_pos += BytesRead;

        UpdateOverlapped();

        return static_cast<uint64>(BytesRead);
    }

    uint64 ReadString(String& _string) final
    {
        _string.clear();
        uint16 len = 0;
        Read(&len, sizeof(len));
        _string.resize(len);
        len *= sizeof(String::value_type);
        return Read(_string.data(), len);
    }

    uint64 Write(const void* _buffer, uint64 _size) final
    {
        AE_ASSERT(IsValid());

        if (_size <= 0) {
            return 0;
        }

        DWORD BytesWritten = 0;

        if (::WriteFile(m_handle, _buffer, static_cast<DWORD>(_size), &BytesWritten, &m_overlapped)
            == FALSE) {
            auto Err = GetLastError();
            if (Err != ERROR_IO_PENDING) {
                return 0;
            }

            BytesWritten = 0;
            if (GetOverlappedResult(m_handle, &m_overlapped, &BytesWritten, TRUE) == 0) {
                return 0;
            }
        }

        m_pos += BytesWritten;

        UpdateOverlapped();

        m_size = m_pos > m_size ? m_pos : m_size;

        return static_cast<uint64>(BytesWritten);
    }

    uint64 WriteString(const String& _string) final
    {
        // We won't be writing strings bigger than uint16.
        uint16 len = (uint16)_string.length();
        Write(&len, sizeof(len));
        Write(_string.data(), len * sizeof(String::value_type));

        return len;
    }

    uint64 CheckSum() final
    {
        uint64 sum   = 0;
        uint64 word  = 0;
        uint64 shift = 0;

        while (Read(&word, sizeof(word)) != 0) {
            sum += (word << shift);
            shift += 8;
            if (shift == 64)
                shift = 0;
        }

        return sum;
    }

  private:
    void UpdateFileStats()
    {
        LARGE_INTEGER LargeInt;
        GetFileSizeEx(m_handle, &LargeInt);
        m_size = LargeInt.QuadPart;
    }

    void UpdateOverlapped()
    {
        ULARGE_INTEGER LI;
        LI.QuadPart             = m_pos;
        m_overlapped.Offset     = LI.LowPart;
        m_overlapped.OffsetHigh = LI.HighPart;
    }

  private:
    HANDLE     m_handle;
    OVERLAPPED m_overlapped;
    uint64     m_pos;
    uint64     m_size;
    bool       m_canRead;
    bool       m_canWrite;
};

class WindowsFileSystem : public FileSystem
{
  public:
    bool FileExists(const tchar* _path) final
    {
        uint32 result = GetFileAttributes(_path);

        if (FAILED(result) || (result & FILE_ATTRIBUTE_DIRECTORY)) {
            return false;
        }

        return true;
    }

    bool DirectoryExists(const tchar* _path) final
    {
        if (String(_path).length() == 0) {
            return true;
        }

        uint32 result = GetFileAttributes(_path);

        if (result == INVALID_FILE_ATTRIBUTES || !(result & FILE_ATTRIBUTE_DIRECTORY)) {
            return false;
        }

        return true;
    }

    bool MakeDirectory(const tchar* _path) final { return CreateDirectory(_path, nullptr) == TRUE; }

    bool SetCWD(const tchar* _path) final { return SetCurrentDirectory(_path) == TRUE; }

    String GetCWD() const final
    {
        uint32 length = MAX_PATH;
        tchar* buffer = new tchar[length];
        GetCurrentDirectoryA(length, buffer);
        String ret(buffer);
        SAFE_DELETE_ARRAY(buffer);
        return ret;
    }

    std::vector<String> GetAllFiles(const tchar* _path, const tchar* _pattern) final
    {
        HANDLE          hFind = INVALID_HANDLE_VALUE;
        WIN32_FIND_DATA ffd;
        String          path(_path);
        path += _pattern;
        std::vector<String> ret;

        hFind = FindFirstFile(path.c_str(), &ffd);
        if (hFind == INVALID_HANDLE_VALUE) {
            return std::vector<String>();
        }

        do {
            if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
                if (!(std::strcmp(ffd.cFileName, ".") == 0
                      || std::strcmp(ffd.cFileName, "..") == 0)) {
                    auto dir = GetAllFiles(ffd.cFileName, _pattern);
                    ret.insert(ret.end(), dir.begin(), dir.end());
                }
            } else {
                tchar  buffer[MAX_PATH];
                String fileName(_path);
                fileName += "\\";
                fileName += ffd.cFileName;
                GetFullPathName(fileName.c_str(), MAX_PATH, buffer, NULL);
                ret.push_back(buffer);
            }
        } while (FindNextFile(hFind, &ffd) != 0);

        return ret;
    }

    FileHandle* OpenRead(const tchar* _path, bool _canWrite) final
    {
        uint32 desiredAccess       = GENERIC_READ;
        uint32 shareMode           = FILE_SHARE_READ | (_canWrite ? FILE_SHARE_WRITE : 0);
        uint32 creationDisposition = OPEN_EXISTING;
        uint32 flags               = FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED;

        HANDLE handle = CreateFile(
          _path, desiredAccess, shareMode, nullptr, creationDisposition, flags, nullptr);

        return new WindowsFileHandle(handle, true, _canWrite);
    }

    FileHandle* OpenWrite(const tchar* _path, bool _append, bool _canRead) final
    {
        uint32 desiredAccess       = GENERIC_WRITE | (_canRead ? GENERIC_READ : 0);
        uint32 shareMode           = _append ? FILE_SHARE_READ : 0;
        uint32 creationDisposition = _append ? OPEN_ALWAYS : CREATE_ALWAYS;
        uint32 flags               = FILE_ATTRIBUTE_NORMAL;

        HANDLE handle = CreateFile(
          _path, desiredAccess, shareMode, nullptr, creationDisposition, flags, nullptr);

        if (handle == INVALID_HANDLE_VALUE) {
            return nullptr;
        }

        WindowsFileHandle* file = new WindowsFileHandle(handle, _canRead, true);

        if (_append) {
            file->Seek(0, SEEK_END);
        }

        return file;
    }
};

FileSystem&
FileSystem::GetPlatformFS()
{
    static WindowsFileSystem fsInstance;
    return fsInstance;
}
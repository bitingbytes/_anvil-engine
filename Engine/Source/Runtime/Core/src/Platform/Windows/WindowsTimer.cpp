#include "Assertion.h"
#include "Platform/Timer.h"
#include "Platform/Windows/WindowsIncludes.h"

class WindowsTimer : public HiResTimer
{
  public:
    WindowsTimer()
      : m_startTime(0)
      , m_totalIdleTime(0)
      , m_pausedTime(0)
      , m_currentTime(0)
      , m_previousTime(0)
      , m_secondsPerCount(0.0)
      , m_deltaTime(0.0)
      , m_isStopped(false)
    {
        LARGE_INTEGER frequency = {};
        AE_ASSERT(QueryPerformanceFrequency(&frequency));
        m_secondsPerCount = 1.0 / (double)frequency.QuadPart;
    }

    double GetTotalTime() const final
    {
        if (m_isStopped) {
            return (m_pausedTime - m_startTime - m_totalIdleTime) * m_secondsPerCount;
        } else {
            return (m_currentTime - m_startTime - m_totalIdleTime) * m_secondsPerCount;
        }
    }
    double GetDeltaTime() const final { return m_deltaTime; }

    void Start() final
    {
        if (m_isStopped) {
            int64 now = 0;
            AE_ASSERT(QueryPerformanceCounter((LARGE_INTEGER*)&now));
            m_totalIdleTime += (now - m_pausedTime);
            m_previousTime = now;
            m_pausedTime   = 0;
            m_isStopped    = false;
        }
    }

    void Stop() final
    {
        if (!m_isStopped) {
            int64 now = 0;
            AE_ASSERT(QueryPerformanceCounter((LARGE_INTEGER*)&now));
            m_pausedTime = now;
            m_isStopped  = true;
        }
    }

    void Reset() final
    {
        int64 now = 0;
        AE_ASSERT(QueryPerformanceCounter((LARGE_INTEGER*)&now));
        m_startTime    = now;
        m_previousTime = now;
        m_pausedTime   = 0;
        m_isStopped    = false;
    }

    void Tick() final
    {
        if (m_isStopped) {
            m_deltaTime = 0.0;
        } else {
            AE_ASSERT(QueryPerformanceCounter((LARGE_INTEGER*)&m_currentTime));
            m_deltaTime    = (m_currentTime - m_previousTime) * m_secondsPerCount;
            m_previousTime = m_currentTime;

            if (m_deltaTime < 0.0) {
                m_deltaTime = 0.0;
            }
        }
    }

  private:
    int64  m_startTime;
    int64  m_totalIdleTime;
    int64  m_pausedTime;
    int64  m_currentTime;
    int64  m_previousTime;
    double m_secondsPerCount;
    double m_deltaTime;
    bool   m_isStopped;
};

HiResTimer&
HiResTimer::GetPlatformHT()
{
    static WindowsTimer hsInstance;
    return hsInstance;
}
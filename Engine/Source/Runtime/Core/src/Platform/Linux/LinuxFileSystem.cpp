#include "Platform/FileSystem.h"
#include "Platform/Linux/LinuxIncludes.h"

class LinuxFileHandle : public FileHandle
{
  public:
    LinuxFileHandle(int32 _handle, const String& _path, bool _canRead, bool _canWrite)
      : m_handle(_handle)
      , m_path(_path)
      , m_pos(0)
      , m_size(0)
      , m_canRead(_canRead)
      , m_canWrite(_canWrite)
    {
        UpdateFileStats();
    }

    ~LinuxFileHandle() { Close(); }

    void Seek(int64 _pos, int32 _mode) final
    {
        AE_ASSERT(IsValid());

        lseek(m_handle, _pos, _mode);
        m_pos = lseek(m_handle, 0, 1);
    }

    void Flush() final
    {
        AE_ASSERT(IsValid());
        fsync(m_handle);
    }

    void Close() final
    {
        if (IsValid()) {
            close(m_handle);
            m_handle = -1;
        }
    }

    bool IsValid() const final { return m_handle != -1; }

    void* GetNativeHandle() final { return &m_handle; }

    uint64 Tell() const final { return m_pos; }

    uint64 GetSize() const final { return m_size; }

    bool IsEOF() const final { return m_pos == m_size; }

    bool IsReadOnly() const final { return m_canRead && !m_canWrite; }

    bool IsWriteOnly() const final { return !m_canRead && m_canWrite; }

    uint64 Read(void* _buffer, uint64 _size) final
    {
        int64 bytes_read = read(m_handle, _buffer, _size);
        return bytes_read;
    }

    uint64 ReadString(String& _string) final
    {
        _string.clear();
        uint16 len = 0;
        Read(&len, sizeof(len));
        _string.resize(len);
        len *= sizeof(String::value_type);
        return Read(_string.data(), len);
    }

    uint64 Write(const void* _buffer, uint64 _size) final
    {
        int64 wrote = write(m_handle, _buffer, _size);
        return wrote;
    }

    uint64 WriteString(const String& _string) final
    { // We won't be writing strings bigger than uint16.
        uint16 len = (uint16)_string.length();
        Write(&len, sizeof(len));
        Write(_string.data(), len * sizeof(String::value_type));

        return len;
    }

  private:
    void UpdateFileStats()
    {
        struct stat sb;
        AE_ASSERT(stat(m_path.c_str(), &sb) == 0);
        m_size = sb.st_size;
    }

  private:
    int32  m_handle;
    String m_path;
    uint64 m_pos;
    uint64 m_size;
    bool   m_canRead;
    bool   m_canWrite;
};

class LinuxFileSystem : public FileSystem
{
  public:
    bool FileExists(const tchar* _path) final { return access(_path, F_OK) != -1; }

    bool DirectoryExists(const tchar* _path) final
    {
        DIR* dir = opendir("mydir");
        if (dir) {
            closedir(dir);
            return true;
        }
        return false;
    }

    FileHandle* OpenRead(const tchar* _path, bool _canWrite) final
    {
        int32 flags = _canWrite ? O_RDWR : O_RDONLY;
        int32 modes = S_IRUSR | S_IRGRP | S_IROTH | (_canWrite ? S_IWUSR : 0);

        int32 handle = open(_path, flags, modes);

        return new LinuxFileHandle(handle, _path, true, _canWrite);
    }

    FileHandle* OpenWrite(const tchar* _path, bool _append, bool _canRead) final
    {
        int32 flags = _canRead ? O_RDWR : O_WRONLY | O_CREAT | (_append ? O_EXCL | O_APPEND : 0);
        int32 modes = S_IRUSR | S_IRGRP | S_IROTH | S_IWUSR;

        int32 handle = open(_path, flags, modes);

        return new LinuxFileHandle(handle, _path, _canRead, true);
    }
};

FileSystem&
FileSystem::GetPlatformFS()
{
    static LinuxFileSystem fsInstance;
    return fsInstance;
}
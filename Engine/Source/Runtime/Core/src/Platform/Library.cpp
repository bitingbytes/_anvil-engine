#include "Platform\Library.h"

Library::Library(const String& _path)
  : m_path(_path)
  , m_error("")
  , m_bIsLoaded(false)
  , m_pHandle(nullptr)
{}

#ifdef AE_WINDOWS
#include "Platform/Windows/WindowsLibrary.h"
#endif // AE_WINDOWS

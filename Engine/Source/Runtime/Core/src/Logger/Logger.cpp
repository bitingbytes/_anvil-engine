#include "Logger/Logger.h"
#include "Assertion.h"
#include "Platform/PlatformIncludes.h"
#include <cstdarg>

Logger::Logger(const String& _category, ELogLevel _level)
  : m_category(_category)
  , m_level(_level)
{
    // m_logFile.reset(FileSystem::GetPlatformFS().OpenWrite(TEXT("LogFile.txt"), true));
    // AE_ASSERT(m_logFile->IsValid());
}

void
Logger::Log(ELogLevel _level, const tchar* _format, ...)
{
    if (m_level < _level) {
        return;
    }

    tchar cBuffer[512];
    memset(cBuffer, 0, 512);

    va_list argptr;
    va_start(argptr, _format);
#if defined(UNICODE) || defined(_UNICODE)
    wvsprintf(cBuffer, _format, argptr);
#else
    vsprintf(cBuffer, _format, argptr);
#endif
    va_end(argptr);

    String sBuffer(cBuffer);
    sBuffer += TEXT("\r\n");

    String Result = m_category + ' ' + GetLogLevelName(_level) + TEXT(": ") + sBuffer;

    OUTPUT_CONSOLE(Result.c_str());
    // m_logFile->Write(Result.c_str(), Result.length());
}

String
Logger::GetLogLevelName(ELogLevel _level) const
{
    switch (_level) {
        case ELogLevel::Fatal:
            return "FATAL";
        case ELogLevel::Error:
            return "ERROR";
        case ELogLevel::Warning:
            return "WARN";
        case ELogLevel::Info:
            return "INFO";
        case ELogLevel::Verbose:
            return "VEB";
    }

    return "";
}

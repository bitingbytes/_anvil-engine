#include "Utils/Parser.h"

bool
Parser::HasParam(const tchar* _command, const tchar* _param)
{
    String temp(_command);
    return temp.find(_param) != temp.npos;
}

bool
Parser::Value(const tchar* _command, const tchar* _param, uint8& _value)
{
    auto data = PrepareForValue(_command, _param);
    if (data == nullptr) {
        return false;
    }
    _value = static_cast<uint8>(std::stoi(data));
    return true;
}

bool
Parser::Value(const tchar* _command, const tchar* _param, uint16& _value)
{
    auto data = PrepareForValue(_command, _param);
    if (data == nullptr) {
        return false;
    }
    _value = static_cast<uint16>(std::stoi(data));
    return true;
}

bool
Parser::Value(const tchar* _command, const tchar* _param, uint32& _value)
{
    auto data = PrepareForValue(_command, _param);
    if (data == nullptr) {
        return false;
    }
    _value = static_cast<uint32>(std::stoi(data));
    return true;
}

bool
Parser::Value(const tchar* _command, const tchar* _param, uint64& _value)
{
    auto data = PrepareForValue(_command, _param);
    if (data == nullptr) {
        return false;
    }
    _value = std::stoull(data);
    return true;
}

bool
Parser::Value(const tchar* _command, const tchar* _param, int8& _value)
{
    auto data = PrepareForValue(_command, _param);
    if (data == nullptr) {
        return false;
    }
    _value = static_cast<int8>(std::stoi(data));
    return true;
}

bool
Parser::Value(const tchar* _command, const tchar* _param, int16& _value)
{
    auto data = PrepareForValue(_command, _param);
    if (data == nullptr) {
        return false;
    }
    _value = static_cast<int16>(std::stoi(data));
    return true;
}

bool
Parser::Value(const tchar* _command, const tchar* _param, int32& _value)
{
    auto data = PrepareForValue(_command, _param);
    if (data == nullptr) {
        return false;
    }
    _value = std::stoi(data);
    return true;
}

bool
Parser::Value(const tchar* _command, const tchar* _param, int64& _value)
{
    auto data = PrepareForValue(_command, _param);
    if (data == nullptr) {
        return false;
    }
    _value = std::stoll(data);
    return true;
}

bool
Parser::Value(const tchar* _command, const tchar* _param, float& _value)
{
    auto data = PrepareForValue(_command, _param);
    if (data == nullptr) {
        return false;
    }
    _value = std::stof(data);
    return true;
}

bool
Parser::Value(const tchar* _command, const tchar* _param, double& _value)
{
    auto data = PrepareForValue(_command, _param);
    if (data == nullptr) {
        return false;
    }
    _value = std::stod(data);
    return true;
}

bool
Parser::Value(const tchar* _command, const tchar* _param, bool& _value, const tchar* _quotes)
{
    String cmd(_command);
    String param(_param);

    if (cmd.empty() || param.empty()) {
        return nullptr;
    }

    auto firstQuote = cmd.find_first_of(_quotes);
    auto lastQuote  = cmd.find_last_of(_quotes);

    if (firstQuote == cmd.npos || lastQuote == cmd.npos) {
        return false;
    }

    cmd.erase(std::remove_if(cmd.begin(), cmd.begin() + firstQuote, std::isspace),
              cmd.begin() + firstQuote);

    auto pos = cmd.find(param);
    if (pos == cmd.npos) {
        return nullptr;
    }

    firstQuote = cmd.find_first_of(_quotes);
    lastQuote  = cmd.find_last_of(_quotes);

    if (firstQuote == cmd.npos || lastQuote == cmd.npos) {
        return false;
    }

    cmd = cmd.substr(firstQuote + 1, (lastQuote - 1) - firstQuote);

    std::for_each(cmd.begin(), cmd.end(), [](char& c) { c = (char)std::tolower((int)c); });

    if (cmd == TEXT("true") || cmd == TEXT("on")) {
        _value = true;
    } else if (cmd == TEXT("false") || cmd == TEXT("off")) {
        _value = false;
    } else {
        return false;
    }

    return true;
}

bool
Parser::Value(const tchar* _command, const tchar* _param, String& _value, const tchar* _quotes)
{
    String cmd(_command);
    String param(_param);

    if (cmd.empty() || param.empty()) {
        return nullptr;
    }

    auto firstQuote = cmd.find_first_of(_quotes);
    auto lastQuote  = cmd.find_last_of(_quotes);

    if (firstQuote == cmd.npos || lastQuote == cmd.npos) {
        auto equal = cmd.find_first_of('=');
        auto end   = cmd.find_first_of(' ', equal);

        if (equal == cmd.npos) {
            return false;
        }

        cmd.insert(equal + 1, "\"");
        cmd.insert(end == cmd.npos ? cmd.length() : end, "\"");
    }

    firstQuote = cmd.find_first_of(_quotes);
    lastQuote  = cmd.find_last_of(_quotes);

    cmd.erase(std::remove_if(cmd.begin(), cmd.begin() + firstQuote, std::isspace),
              cmd.begin() + firstQuote);

    auto pos = cmd.find(param);
    if (pos == cmd.npos) {
        return nullptr;
    }

    firstQuote = cmd.find_first_of(_quotes);
    lastQuote  = cmd.find_last_of(_quotes);

    if (firstQuote == cmd.npos || lastQuote == cmd.npos) {
        return false;
    }

    _value = cmd.substr(firstQuote + 1, --lastQuote - firstQuote);

    return true;
}

tchar*
Parser::PrepareForValue(const tchar* _command, const tchar* _param)
{
    String cmd(_command);
    String param(_param);

    if (cmd.empty() || param.empty()) {
        return nullptr;
    }

    cmd.erase(std::remove_if(cmd.begin(), cmd.end(), std::isspace), cmd.end());

    auto pos = cmd.find(param);
    if (pos == cmd.npos) {
        return nullptr;
    }

    return cmd.data() + pos + param.length();
}

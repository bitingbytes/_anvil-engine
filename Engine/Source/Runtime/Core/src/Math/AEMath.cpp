#include "Math/AEMath.h"
#include "Math/Color.h"
#include "Math/Vector2D.h"
#include "Math/Vector3D.h"
#include "Math/Vector4D.h"
#include <CorePCH.pch.h>

AE_CORE_API const Vector2D Vector2D::Zero(0.0f, 0.0f);
AE_CORE_API const Vector2D Vector2D::One(1.0f, 1.0f);
AE_CORE_API const Vector2D Vector2D::Right(1.0f, 0.0f);
AE_CORE_API const Vector2D Vector2D::Left(-1.0f, 0.0f);
AE_CORE_API const Vector2D Vector2D::Up(0.0f, 0.0f);
AE_CORE_API const Vector2D Vector2D::Down(0.0f, 0.0f);

AE_CORE_API const Vector3D Vector3D::Zero(0.0f, 0.0f, 0.0f);
AE_CORE_API const Vector3D Vector3D::One(1.0f, 1.0f, 1.0f);
AE_CORE_API const Vector3D Vector3D::Right(1.0f, 0.0f, 0.0f);
AE_CORE_API const Vector3D Vector3D::Left(-1.0f, 0.0f, 0.0f);
AE_CORE_API const Vector3D Vector3D::Up(0.0f, 0.0f, 1.0f);
AE_CORE_API const Vector3D Vector3D::Down(0.0f, 0.0f, -1.0f);
AE_CORE_API const Vector3D Vector3D::Forward(0.0f, 1.0f, 0.0f);
AE_CORE_API const Vector3D Vector3D::Backward(0.0f, -1.0f, 0.0f);

AE_CORE_API const Vector4D Vector4D::Zero(0.0f, 0.0f, 0.0f, 0.0f);
AE_CORE_API const Vector4D Vector4D::One(1.0f, 1.0f, 1.0f, 0.0f);
AE_CORE_API const Vector4D Vector4D::Right(1.0f, 0.0f, 0.0f, 0.0f);
AE_CORE_API const Vector4D Vector4D::Left(-1.0f, 0.0f, 0.0f, 0.0f);
AE_CORE_API const Vector4D Vector4D::Up(0.0f, 0.0f, 1.0f, 0.0f);
AE_CORE_API const Vector4D Vector4D::Down(0.0f, 0.0f, -1.0f, 0.0f);
AE_CORE_API const Vector4D Vector4D::Forward(0.0f, 1.0f, 0.0f, 0.0f);
AE_CORE_API const Vector4D Vector4D::Backward(0.0f, -1.0f, 0.0f, 0.0f);

AE_CORE_API const Color Color::Black(0.0f, 0.0f, 0.0f, 1.0f);
AE_CORE_API const Color Color::White(1.0f, 1.0f, 1.0f, 1.0f);
AE_CORE_API const Color Color::Red(1.0f, 0.0f, 0.0f, 1.0f);
AE_CORE_API const Color Color::Green(0.0f, 1.0f, 0.0f, 1.0f);
AE_CORE_API const Color Color::Blue(0.0f, 0.0f, 1.0f, 1.0f);
AE_CORE_API const Color Color::Yellow(1.0f, 1.0f, 0.0f, 1.0f);
AE_CORE_API const Color Color::Magenta(1.0f, 0.0f, 1.0f, 1.0f);
AE_CORE_API const Color Color::Cyan(0.0f, 1.0f, 1.0f, 1.0f);
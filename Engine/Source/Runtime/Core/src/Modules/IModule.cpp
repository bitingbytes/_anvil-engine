#include "Modules/IModule.h"
#include "Platform/Library.h"
#include "CoreGlobals.h"	

ModuleManager::~ModuleManager()
{
    for (auto it = m_registeredModules.begin(); it != m_registeredModules.end(); it++) {
        auto mod = (*it).second;
        mod->Shutdown();
        delete mod;
        mod = nullptr;
    }
}

ModuleManager&
ModuleManager::GetInstance()
{
    static ModuleManager s_instance;
    return s_instance;
}

void
ModuleManager::RegisterModule(const String& _name, IModule* _module)
{
    if (m_registeredModules.find(_name) == m_registeredModules.end()) {
        AE_VERB(LogInit, TEXT("Registering module %s"), _name.c_str());
        m_registeredModules.emplace(_name, _module);
    }
}

IModule*
ModuleManager::LoadModule(const String& _name)
{
    AE_INFO(LogInit, TEXT("Loading Module %s"), _name.c_str());
    if (auto fmod = m_registeredModules.find(_name); fmod != m_registeredModules.end()) {
        return fmod->second;
    }
#ifndef AE_MONOLITHIC
    Library lib(_name);

    FModuleInitializerPtr Initializer = lib.Resolve<FModuleInitializerPtr>("InitializeModule");

    if (!Initializer) {
        AE_ERROR(LogInit, TEXT("Could not load module %s - InitializeModule not found"), _name.c_str());
        return nullptr;
    }

    IModule* mod = Initializer();
    if (!mod) {
        return nullptr;
    }

    RegisterModule(_name, mod);

    return mod;
#else
    return nullptr;
#endif // !AE_MONOLITHIC
}

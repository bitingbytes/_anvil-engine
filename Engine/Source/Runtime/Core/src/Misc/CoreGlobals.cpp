#include "CoreGlobals.h"

CREATE_LOGGER(LogInit);

bool g_IsInitialized  = false;
bool g_IsShuttingDown = false;
bool g_IsPaused       = false;
bool g_IsEditor       = false;

std::thread::id g_MainThreadID;

AE_CORE_API bool
IsMainThread()
{
    return std::this_thread::get_id() == g_MainThreadID;
}
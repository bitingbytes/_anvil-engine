project(AnvilCore)

set(HEADER_FILES 
    include/CorePCH.pch.h
    include/Core.h
    include/Types.h
    include/Assertion.h
    include/CoreGlobals.h
    include/Utils/LibraryUtils.h
    include/Modules/IModule.h
    include/Utils/NonCopyable.h
    include/Utils/DataUtils.h
    include/Utils/Parser.h
    include/Containers/AEGUID.h
    include/Containers/AString.h
    include/Containers/Version.h
    include/Math/AEMath.h
	include/Math/Vector2D.h
	include/Math/Vector3D.h
    include/Math/Vector4D.h
    include/Math/Color.h
    include/Logger/Logger.h
    include/Platform/PlatformIncludes.h
    include/Platform/Library.h
    include/Platform/FileSystem.h
    include/Platform/Timer.h
    include/Platform/${CMAKE_SYSTEM_NAME}/${CMAKE_SYSTEM_NAME}Includes.h
    include/Platform/${CMAKE_SYSTEM_NAME}/${CMAKE_SYSTEM_NAME}Library.h
)

set(SOURCE_FILES
    src/CorePCH.pch.cpp
    src/Modules/IModule.cpp
    src/Math/AEMath.cpp
    src/Misc/CoreGlobals.cpp
    src/Logger/Logger.cpp
    src/Platform/Library.cpp
    src/Utils/Parser.cpp
    src/Containers/AEGUID.cpp
    src/Platform/${CMAKE_SYSTEM_NAME}/${CMAKE_SYSTEM_NAME}FileSystem.cpp
    src/Platform/${CMAKE_SYSTEM_NAME}/${CMAKE_SYSTEM_NAME}Timer.cpp
)

add_library(${PROJECT_NAME} ${BUILD_MODE}
    ${HEADER_FILES}
    ${SOURCE_FILES}
)

target_include_directories(${PROJECT_NAME}
    PUBLIC
    $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:include>
)

set(PROJECT_MIN_NAME_UPPED "CORE")

set(LIBRARY_DEPENDENCIES "")

include(SetupExportation)
include(SetupAnvilModule)
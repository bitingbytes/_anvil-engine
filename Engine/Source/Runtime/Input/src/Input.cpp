#include "Input.h"
#include "CoreGlobals.h"

CREATE_LOGGER(LogModule);

IInput* g_InputManager;

AE_INPUT_API bool
Input::Initialize()
{
    IInputModule* InputModule = static_cast<IInputModule*>(ModuleManager::GetInstance().LoadModule("AnvilXInput"));
    if (!InputModule) {
        AE_ERROR(LogModule, TEXT("Could not load InputModule"));
        return false;
    }
    if (!InputModule->CreateInputModule()) {
        AE_ERROR(LogModule, TEXT("Could not initialize InputModule"));
        return false;
    }
    return true;
}

AE_INPUT_API void
Input::Tick()
{
    g_InputManager->Tick();
}

AE_INPUT_API void
Input::Shutdown()
{
	if (g_InputManager) {
        SAFE_DELETE(g_InputManager);
	}
}

AE_INPUT_API IGamepad*
             Input::Gamepad(uint32 _playerID)
{
    return g_InputManager->GetGamepad(_playerID);
}

/**
 * @file Input.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2019-11-22
 *
 * @copyright Copyright (c) 2019
 *
 */

#pragma once

#include "IInput.h"
#include "Modules/IModule.h"

CREATE_LOGGER_EXTERN(LogModule, All);

class IInputModule : public IModule
{
  public:
    virtual IInput* CreateInputModule() = 0;
};

extern AE_INPUT_API IInput* g_InputManager;

namespace Input {
AE_INPUT_API bool
Initialize();

AE_INPUT_API void
Tick();

AE_INPUT_API void
Shutdown();

AE_INPUT_API IGamepad*
             Gamepad(uint32 _playerID = 0);
}
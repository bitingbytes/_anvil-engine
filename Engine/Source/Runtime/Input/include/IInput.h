/**
 * @file Input.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2019-11-21
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once

#include "Types.h"

class IGamepad;

enum class EKeyState
{
    StillReleased,
    JustPressed,
    StillPressed,
    JustReleased
};

enum class EButtonCode : uint16
{
    DPad_UP        = 0x0001,
    DPad_Down      = 0x0002,
    DPad_Left      = 0x0004,
    DPad_Right     = 0x0008,
    Start          = 0x0010,
    Back           = 0x0020,
    Left_Thumb     = 0x0040,
    Right_Thumb    = 0x0080,
    Left_Shoulder  = 0x0100,
    Right_Shoulder = 0x0200,
    A              = 0x1000,
    B              = 0x2000,
    X              = 0x4000,
    Y              = 0x8000,
};

class IInput
{
  public:
    virtual ~IInput() = default;

    virtual void Tick() = 0;

    virtual IGamepad* GetGamepad(uint32 _playerID) = 0;
};

class IGamepad
{
  public:
    virtual ~IGamepad()                                                                = default;
    virtual void            Tick()                                                     = 0;
    virtual void            Vibrate(float _left, float _right, float _duration = 1.0f) = 0;
    virtual const bool      IsPressed(EButtonCode _button) const                       = 0;
    virtual const bool      WasPressed(EButtonCode _button) const                      = 0;
    virtual const bool      IsButtonUp(EButtonCode _button) const                      = 0;
    virtual const bool      IsButtonDown(EButtonCode _button) const                    = 0;
    virtual const EKeyState GetButtonState(EButtonCode _button) const                  = 0;
};
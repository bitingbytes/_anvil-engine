project(AnvilLaunch)

set(HEADER_FILES 
    include/LaunchPCH.pch.h
    include/EngineLoop.h
)

set(SOURCE_FILES
    src/LaunchPCH.pch.cpp
    src/Launch.cpp
    src/EngineLoop.cpp
    src/${CMAKE_SYSTEM_NAME}/${CMAKE_SYSTEM_NAME}Launch.cpp
)

if(WIN32)
    include(generate_product_version)
    generate_product_version(
        VersionFilesOutputVariable
        NAME ${PROJECT_NAME}
        ICON ${PROJECT_SOURCE_DIR}/resources/anvil.ico
        VERSION_MAJOR 0
        VERSION_MINOR 0
        VERSION_PATCH 1
        VERSION_REVISION ${BUILD_REVISION}
        COMPANY_NAME "Eremita Studios"
    )

    add_executable(${PROJECT_NAME} WIN32
        ${HEADER_FILES}
        ${SOURCE_FILES}
        ${VersionFilesOutputVariable}
    )

else()
    add_executable(${PROJECT_NAME}
        ${HEADER_FILES}
        ${SOURCE_FILES}
        ${VersionFilesOutputVariable}
    )
endif()


source_group("Resources" FILES ${VersionFilesOutputVariable})

target_include_directories(${PROJECT_NAME}
    PUBLIC
    $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:include>
)

set(LIBRARY_DEPENDENCIES AnvilCore AnvilInput)

set(PROJECT_MIN_NAME_UPPED "LAUNCH")

include(SetupExportation)
include(SetupAnvilModule)

target_link_libraries(${PROJECT_NAME} PRIVATE ${LIBRARY_DEPENDENCIES})

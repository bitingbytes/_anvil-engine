/**
 * @file EngineLoop.h
 * @author Brian Batista (brianbatista93@gmail.com)
 * @brief
 * @version 0.1
 * @date 2019-11-22
 *
 * @copyright Copyright (c) 2019
 *
 */

#pragma once

#include "Types.h"
#include "CoreGlobals.h"

class EngineLoop
{
  public:
    int32 PreInitialize();
    int32 Initialize();
    void  Tick();
    void  Shutdown();
};

extern EngineLoop g_EngineLoop;
#include "EngineLoop.h"
#include "Input.h"

int32
EngineLoop::PreInitialize()
{
    /* Load and Init Input Module */
    {
		if (!Input::Initialize()) {
            return EXIT_FAILURE;
		}
    }

    return EXIT_SUCCESS;
}

int32
EngineLoop::Initialize()
{
    return int32();
}

void
EngineLoop::Tick()
{
    Input::Tick();
}

void
EngineLoop::Shutdown()
{
    g_IsShuttingDown = true;

    Input::Shutdown();
}

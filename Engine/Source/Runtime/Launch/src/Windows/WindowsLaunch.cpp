#include "Platform/Windows/WindowsIncludes.h"
#include "Types.h"

extern "C"
{
    // https://docs.nvidia.com/gameworks/content/technologies/desktop/optimus.htm
    __declspec(dllexport) uint32 NvOptimusEnablement = 0x00000001;

    // https://gpuopen.com/amdpowerxpressrequesthighperformance/
    __declspec(dllexport) int32 AmdPowerXpressRequestHighPerformance = 1;
}

extern int32
MainThread(const char* _cmds, HINSTANCE _hInstance, HINSTANCE _prevHInstance, int32 _cmdShow);

int32
ProcessException(LPEXCEPTION_POINTERS ExceptionInfo);

int32 WINAPI
      WinMain(_In_ HINSTANCE _instance, _In_opt_ HINSTANCE _prevInstance, _In_ char*, _In_ int32 _cmdShow)
{
    SetThreadDpiAwarenessContext(DPI_AWARENESS_CONTEXT_PER_MONITOR_AWARE_V2);

    HRESULT hr = CoInitializeEx(nullptr, COINITBASE_MULTITHREADED);
    if (FAILED(hr)) {
        return EXIT_FAILURE;
    }

#if AE_DEBUG
    _CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
    _CrtSetReportMode(_CRT_ASSERT, _CRTDBG_MODE_DEBUG);
    _CrtSetDebugFillThreshold(0);
#endif

    char* cmds      = GetCommandLineA();
    int32 errorCode = 1;
    __try {
        errorCode = MainThread(cmds, _instance, _prevInstance, _cmdShow);
    } __except (GetExceptionInformation(), EXCEPTION_CONTINUE_SEARCH) {
        (void)0;
    }

    return errorCode;
}

int32
ProcessException(LPEXCEPTION_POINTERS _exceptionInfo)
{
    UNREFERENCED_PARAMETER(_exceptionInfo);
    return EXCEPTION_EXECUTE_HANDLER;
}

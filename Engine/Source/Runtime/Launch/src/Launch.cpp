#include "CoreGlobals.h"
#include "EngineLoop.h"
#include "Platform/PlatformIncludes.h"
#include "Types.h"

EngineLoop g_EngineLoop;

int32
EnginePreInitialize()
{
    int32 errorCode = g_EngineLoop.PreInitialize();
    return (errorCode);
}

int32
EngineInitialize()
{
    int32 errorCode = g_EngineLoop.Initialize();
    return (errorCode);
}

void
EngineTick()
{
    g_EngineLoop.Tick();
}

void
EngineShutdown()
{
    g_EngineLoop.Shutdown();
}

#ifdef AE_WINDOWS
int32
MainThread(const char* _cmds, HINSTANCE _hInstance, HINSTANCE _prevHInstance, int32 _cmdShow)
#else
int32
MainThread(const char* _cmds)
#endif
{
#ifdef AE_WINDOWS
    UNREFERENCED_PARAMETER(_hInstance);
    UNREFERENCED_PARAMETER(_prevHInstance);
    UNREFERENCED_PARAMETER(_cmdShow);
#endif // AE_WINDOWS

    struct _EngineLoopGuard
    {
        ~_EngineLoopGuard() { EngineShutdown(); }
    } EngineLoopGuard;

    int32 errorCode = EnginePreInitialize();

    if (errorCode != 0 || g_IsShuttingDown) {
        return errorCode;
    }

    errorCode = EngineInitialize();

    while (!g_IsShuttingDown) {
        EngineTick();
    }

    return errorCode;
}
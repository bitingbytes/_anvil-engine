#include "AXGamepad.h"

AXGamepad::AXGamepad(uint32 _playerID, const XINPUT_STATE& _state, const XINPUT_BATTERY_INFORMATION& _battery)
  : m_id(_playerID)
  , m_curState(_state)
  , m_prevState(_state)
  , m_battery(_battery)
{}

AXGamepad::~AXGamepad() {}

void
AXGamepad::Tick()
{
    memcpy(&m_prevState, &m_curState, sizeof(XINPUT_STATE));

    m_curState = {0};
    XInputGetState(m_id, &m_curState);
}

void
AXGamepad::Vibrate(float _left, float _right, float _duration)
{
    std::thread vibrationThread([&]() {
        float left     = std::max(0.0f, std::min(1.0f, _left));
        float right    = std::max(0.0f, std::min(1.0f, _right));
        float duration = std::max(0.0f, _duration);

        XINPUT_VIBRATION vibration = {0};
        vibration.wLeftMotorSpeed  = uint16(left * 65535);
        vibration.wRightMotorSpeed = uint16(right * 65535);
        XInputSetState(m_id, &vibration);
        Sleep(static_cast<uint32>(duration * 1000.0f));
        vibration = {0};
        XInputSetState(m_id, &vibration);
    });

    vibrationThread.detach();
}

const bool
AXGamepad::IsPressed(EButtonCode _button) const
{
    return (m_curState.Gamepad.wButtons & (uint16)_button) != 0;
}

const bool
AXGamepad::WasPressed(EButtonCode _button) const
{
    return (m_prevState.Gamepad.wButtons & (uint16)_button) != 0;
}

const bool
AXGamepad::IsButtonUp(EButtonCode _button) const
{
    return GetButtonState(_button) == EKeyState::JustReleased;
}

const bool
AXGamepad::IsButtonDown(EButtonCode _button) const
{
    return GetButtonState(_button) == EKeyState::JustPressed;
}

const EKeyState
AXGamepad::GetButtonState(EButtonCode _button) const
{
    if (WasPressed(_button)) {
        if (IsPressed(_button)) {
            return EKeyState::StillPressed;
        } else {
            return EKeyState::JustReleased;
        }
    } else if (IsPressed(_button)) {
        return EKeyState::JustPressed;
    } else {
        return EKeyState::StillReleased;
    }
}

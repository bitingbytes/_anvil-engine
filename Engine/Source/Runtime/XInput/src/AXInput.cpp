#include "AXInput.h"

CREATE_LOGGER(LogXInput);

CREATE_MODULE(AnvilXInputModule, AnvilXInput);

IInput*
AnvilXInputModule::CreateInputModule()
{
    g_InputManager = new AXInput();
    return g_InputManager;
}

AXInput::AXInput()
  : m_gamepads{nullptr, nullptr, nullptr, nullptr}
{
    for (uint32 i = 0; i < XUSER_MAX_COUNT; i++) {
        m_gamepads[i] = new AXGamepad(i, {0}, {0});
    }
}

AXInput::~AXInput()
{
    for (int32 i = 0; i < XUSER_MAX_COUNT; i++) {
        SAFE_DELETE(m_gamepads[i]);
    }
}

void
AXInput::Tick()
{
    for (uint32 i = 0; i < XUSER_MAX_COUNT; i++) {
        m_gamepads[i]->Tick();
    }
}

IGamepad*
AXInput::GetGamepad(uint32 _playerID)
{
    AE_ASSERT(_playerID < XUSER_MAX_COUNT);
    return m_gamepads[_playerID];
}

String
AXInput::BatteryLevelToString(uint8 _level)
{
    switch (_level) {
        case BATTERY_LEVEL_EMPTY:
            return "Empty";
        case BATTERY_LEVEL_LOW:
            return "Low";
        case BATTERY_LEVEL_MEDIUM:
            return "Medium";
        case BATTERY_LEVEL_FULL:
            return "Full";
    }

    return "Unknown";
}

/**
 * @file AXInput.h
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2019-11-21
 *
 * @copyright Copyright (c) 2019
 *
 */

#pragma once

#include "AXGamepad.h"
#include "Input.h"
#include "Logger/Logger.h"
#include "Modules/IModule.h"
#include <Windows.h>
#include <Xinput.h>

CREATE_LOGGER_EXTERN(LogXInput, All);

class AXInput;

class AnvilXInputModule : public IInputModule
{
  public:
    IInput* CreateInputModule() final;
};

class AXInput : public IInput
{
  public:
    AXInput();

    ~AXInput();

    void Tick() final;

    IGamepad* GetGamepad(uint32 _playerID) final;

  private:
    String BatteryLevelToString(uint8 _level);

    AXGamepad* m_gamepads[XUSER_MAX_COUNT];
};
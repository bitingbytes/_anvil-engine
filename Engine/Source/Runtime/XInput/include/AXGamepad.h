/**
 * @file AXGamepad.h
 * @author your name (you@domain.com)
 * @brief
 * @version 0.1
 * @date 2019-11-21
 *
 * @copyright Copyright (c) 2019
 *
 */
#pragma once

#include "Input.h"
#include "Platform/Windows/WindowsIncludes.h"
#include <Xinput.h>

class AXGamepad : public IGamepad
{
  public:
    AXGamepad(uint32 _playerID, const XINPUT_STATE& _state, const XINPUT_BATTERY_INFORMATION& _battery);

    ~AXGamepad();

    // Inherited via IGamepad
    virtual void            Tick() override;
    virtual void            Vibrate(float _left, float _right, float _duration) override;
    virtual const bool      IsPressed(EButtonCode _button) const override;
    virtual const bool      WasPressed(EButtonCode _button) const override;
    virtual const bool      IsButtonUp(EButtonCode _button) const override;
    virtual const bool      IsButtonDown(EButtonCode _button) const override;
    virtual const EKeyState GetButtonState(EButtonCode _button) const override;

  private:
    uint32                     m_id;
    XINPUT_STATE               m_curState;
    XINPUT_STATE               m_prevState;
    XINPUT_BATTERY_INFORMATION m_battery;
};
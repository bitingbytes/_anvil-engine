project(AnvilTest)

add_executable(${PROJECT_NAME} include/test.h src/test.cpp)

target_include_directories(${PROJECT_NAME}
    PUBLIC
    $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:include>
)

set(LIBRARY_DEPENDENCIES 
    AnvilCore
    AnvilInput
)

include(SetupExportation)

target_link_libraries(${PROJECT_NAME} PUBLIC ${LIBRARY_DEPENDENCIES} AnvilXInput)
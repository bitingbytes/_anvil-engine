#include "test.h"
#include "IInput.h"

CREATE_LOGGER(LogTest);

int32
main()
{
    HRESULT hr = CoInitializeEx(nullptr, COINITBASE_MULTITHREADED);
    if (FAILED(hr))
        return EXIT_FAILURE;

    IInputModule* InputModule = static_cast<IInputModule*>(ModuleManager::GetInstance().LoadModule("AnvilXInput"));

    if (InputModule->CreateInputModule()) {
        while (true) {
            Input::Tick();
            if (Input::Gamepad()->IsButtonUp(EButtonCode::Y)) {
                Input::Gamepad()->Vibrate(0.0f, 1.0f, 0.05f);               
            }
            if (Input::Gamepad()->IsButtonUp(EButtonCode::Start)) {
                break;
            }

        }
    }

    AE_INFO(LogTest, "Oi meu nome é %s e tenho %d anos.", "Brian", 26);

    return 0;
}
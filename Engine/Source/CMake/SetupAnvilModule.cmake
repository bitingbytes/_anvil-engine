add_precompiled_header(${PROJECT_NAME} ${PROJECT_SOURCE_DIR}/include/${PROJECT_MIN_NAME}PCH.pch.h ${PROJECT_SOURCE_DIR}/src/${PROJECT_MIN_NAME}PCH.pch.cpp)

set_property(TARGET ${PROJECT_NAME} PROPERTY FOLDER "Engine")